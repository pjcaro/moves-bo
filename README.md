# MOVES - Backoffice

## Características del servidor
Actualmente, el proyecto corre en un servidor que cuenta con las siguientes características:

- Ubuntu 20.04.2 LTS
- Node (v14.15.0)
- Nginx (v1.18.0)
- pm2 (v5.1.0)



## Dependencias
Todas las dependencias están declaradas en el archivo `package.json` ubicado en la raíz del proyecto.



## Variables de entorno
Las variables de entorno necesarias están listadas en el archivo `.env.example` ubicado en la raíz del proyecto.  
Estas variables de entorno deben estar definidas en archivos cuyo nombre sea `.env.{environment}`. Es necesario tener el archivo correspondiente en la raíz del proyecto al momento de realizar la build del proyecto, dependiendo del ambiente.   
Los ambientes disponibles son `development` y `staging`



## Configuración de ambiente

### Configuración del webserver (nginx)
1. Teniendo instalado `nginx` en el servidor, ubicarse en el directorio `/etc/nginx/sites-enabled`
2. Crear un archivo de configuración. El nombre que usamos en este caso fue `moves-bo`. Para una referencia sobre este archivo de configuración, está disponible el archivo `nginx.conf` en la raíz del proyecto.
3. Ejecutar el comando `sudo nginx -t` para comprobar que el archivo de configuración no tiene errores de sintaxis.
    1. En caso de que el resultado sea `ok`, ejecutar el comando `sudo service nginx reload` para actualizar nginx con los nuevos cambios al archivo de configuración.
    2. En caso de que el resultado no sea `ok`, realizar las correcciones necesarias al archivo de configuración y repetir el paso 2.
    


## Deploy
Para crear una build de la aplicación, ejecutar el comando `npm run build:{entorno}`. Esto creará un directorio denominado `build` en la raíz del proyecto, el cual contiene los archivos de la build listos para el deploy.


El deploy de la build se puede hacer de dos maneras:

- Copiar los archivos locales al servidor usando `scp`,
- O bien usar un cliente SSH como [MobaXterm](https://mobaxterm.mobatek.net/) para conectarse al servidor y subir los archivos mediante SFTP.

En ambos casos el resultado es el mismo, copiar los archivos de la build al directorio donde se quiere realizar el deploy.  
En este caso, el directorio que usamos es `/var/www/moves-bo`, y el directorio que contiene la build se denomina `html`.

### Copiar los archivos de la build al servidor con `scp`

```bash
scp -r -i /path/key.pem [/path/file-to-copy] username@instance-public-dns-name:/path/deploy-folder
```

Ejemplo:

```bash
scp -r -i key.pem moves-bo/build/* ubuntu@ec2-3-210-192-201.compute-1.amazonaws.com:/var/www/moves-bo/html
```
