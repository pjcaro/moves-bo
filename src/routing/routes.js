import withAuthentication from '../HOC/withAuthentication';
import {
	LoginScreen,
	PageNotFound,
	VisitorsScreen,
	ResetPasswordScreen,
	NewVisitorScreen,
	AdminsScreen,
	NewAdminScreen,
	EditVisitorScreen,
	VehiclesScreen,
	NewVehicleScreen,
	DashboardScreen
} from '../screens';
import EditAdminScreen from '../screens/edit-admin';
import EditVehicleScreen from '../screens/edit-vehicle';
import { SUPER_ADMIN } from '../utils/constants';
import {
	ADMINS_ROUTE,
	DASHBOARD_ROUTE,
	EDIT_ADMIN_ROUTE,
	EDIT_VEHICLE_ROUTE,
	EDIT_VISTOR_ROUTE,
	LOGIN_ROUTE,
	NEW_ADMIN_ROUTE,
	NEW_VEHICLE_ROUTE,
	NEW_VISITOR_ROUTE,
	RESET_PASSWORD_ROUTE,
	VEHICLES_ROUTE,
	VISITORS_ROUTE
} from './paths';

const routes = [
	{
		path: LOGIN_ROUTE,
		component: LoginScreen,
		exact: true
	},
	{
		path: RESET_PASSWORD_ROUTE,
		component: ResetPasswordScreen,
		exact: true
	},
	{
		path: VISITORS_ROUTE,
		component: withAuthentication( VisitorsScreen ),
		exact: true
	},
	{
		path: NEW_VISITOR_ROUTE,
		component: withAuthentication( NewVisitorScreen ),
		exact: true
	},
	{
		path: EDIT_VISTOR_ROUTE,
		component: withAuthentication( EditVisitorScreen ),
		exact: true
	},
	{
		path: ADMINS_ROUTE,
		component: withAuthentication( AdminsScreen ),
		superAdmin: true,
		exact: true
	},
	{
		path: NEW_ADMIN_ROUTE,
		component: withAuthentication( NewAdminScreen ),
		superAdmin: true,
		exact: true
	},
	{
		path: EDIT_ADMIN_ROUTE,
		component: withAuthentication( EditAdminScreen ),
		superAdmin: true,
		exact: true
	},
	{
		path: VEHICLES_ROUTE,
		component: withAuthentication( VehiclesScreen ),
		exact: true
	},
	{
		path: NEW_VEHICLE_ROUTE,
		component: withAuthentication( NewVehicleScreen ),
		exact: true
	},
	{
		path: EDIT_VEHICLE_ROUTE,
		component: withAuthentication( EditVehicleScreen ),
		exact: true
	},
	{
		path: DASHBOARD_ROUTE,
		component: withAuthentication( DashboardScreen ),
		exact: true
	},
	{
		path: '*',
		component: PageNotFound
	}
];

const getRoutes = ( role ) => routes.filter( ( route ) => (
	( route.superAdmin && role === SUPER_ADMIN ) || !route.superAdmin )
);

export default getRoutes;
