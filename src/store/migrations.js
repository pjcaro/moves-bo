import { initialState as ISReport } from '../reducers/reports';

export const migrations = {
	9: ( state ) => ( {
		...state,
		reports: {
			...state.reports,
			areasCovered: ISReport.areasCovered,
			hoursOfUse: ISReport.hoursOfUse,
			avoidedEmission: ISReport.avoidedEmission,
			energeticConsumptionFiltered: ISReport.energeticConsumption,
			expenses: ISReport.expenses,
			loadDistribution: ISReport.loadDistribution
		}
	} ),
	10: ( state ) => ( {
		...state,
		reports: {
			kilometers: ISReport.kilometers
		}
	} ),
	11: ( state ) => ( {
		...state,
		reports: {
			travelEvents: ISReport.travelEvents
		}
	} )
};
