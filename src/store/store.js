import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { middleware as reduxPackMiddleware } from 'redux-pack';
import { persistStore, persistReducer, createMigrate } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { createBrowserHistory } from 'history';
import loggerMiddleware from './middlewares/logger';
import { rootReducer } from '../reducers';
import { migrations } from './migrations';

export const history = createBrowserHistory( { forceRefresh: true } );

const persistConfig = {
	key: 'root',
	version: 10,
	blacklist: [ 'reports' ],
	storage,
	migrate: createMigrate( migrations, { debug: false } )
};

const middlewares = [
	thunk,
	reduxPackMiddleware
];

if ( process.env.NODE_ENV === 'development' ) {
	middlewares.push( loggerMiddleware );
}

const persistedReducer = persistReducer( persistConfig, rootReducer );

export const store = createStore(
	persistedReducer,
	applyMiddleware( ...middlewares )
);

export const persistor = persistStore( store );
