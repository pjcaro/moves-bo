import { Report, ReportHeatMap, User } from '../entity';

export const initializeUser = () => User.fromJSON();
export const initializeReport = () => Report.fromJSON();
export const initilizeReportHeatMap = () => ReportHeatMap.fromJSON();
