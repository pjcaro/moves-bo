import {
	User, Visitor, Admin, VehicleType, BrandModel, Report, Point, ReportHeatMap
} from '../entity';
import ReportInfo from '../entity/reportInfo';
import Vehicle from '../entity/vehicle';

export const parserVisitor = ( item ) => {
	const {
		id, firstName, lastName, email, weight, idCard, status, vehicleLicense
	} = item;

	return Visitor.fromJSON( {
		id, firstName, lastName, email, weight, idCard, status, vehicleLicense
	} );
};

export const parserUser = ( item ) => {
	const {
		id, firstName, lastName, email, role
	} = item;

	return User.fromJSON( {
		id, firstName, lastName, email, role
	} );
};

export const parserAdmin = ( item ) => {
	const {
		id, firstName, lastName, email, status, role
	} = item;

	return Admin.fromJSON( {
		id, firstName, lastName, email, status, role
	} );
};

export const parserVehicle = ( item ) => {
	const {
		id, license, type, brandModel, visitor
	} = item;

	return Vehicle.fromJSON( {
		id, license, type, brandModel, visitor
	} );
};

export const parserVehicletypes = ( item ) => {
	const { id, description } = item;

	return VehicleType.fromJSON( { id, description } );
};

export const parserBrandModel = ( item ) => {
	const { id, description } = item;

	return BrandModel.fromJSON( { id, description } );
};

export const parserReports = ( item = {} ) => {
	const { yAxis: axisY = '', data } = item;

	return Report.fromJSON( { axisY, data } );
};

export const parserReportsHeatMap = ( item = {} ) => {
	const { data, center, zoom } = item;

	return ReportHeatMap.fromJSON( { data, center, zoom } );
};

export const parserReportInfo = ( item ) => {
	const { title, value, measurementUnit } = item;

	return ReportInfo.fromJSON( { title, value, measurementUnit } );
};

export const parserPoint = ( ( item ) => {
	const { latitude: lat, longitude: lng } = item;

	return Point.fromJSON( { lat, lng } );
} );
