import { message } from 'antd';
import { ERROR, SUCCESS, WARNING } from './constants';

export const showMessage = ( { type = SUCCESS, msg } ) => {
	if ( type === SUCCESS ) return message.success( msg );
	if ( type === ERROR ) return message.error( msg );
	if ( type === WARNING ) return message.warning( msg );
	return null;
};
