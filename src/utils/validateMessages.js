/* eslint-disable no-template-curly-in-string */
export const validateMessages = {
	required: '${label} es requerido',
	types: {
		email: 'El ${label} ingresado no es válido'
	}
};
