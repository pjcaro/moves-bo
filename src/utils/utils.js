import {
	ACTIVE, ACTIVE_STATUS, INACTIVE, INACTIVE_STATUS, INVITED, INVITED_STATUS, NOT_RESULT_FOUND
} from './constants';

const names = {
	firstName: 'Nombre',
	lastName: 'Apellido',
	email: 'Email',
	weight: 'Peso',
	idCard: 'Cédula',
	status: 'Estado',
	role: 'Rol',
	type: 'Tipo',
	license: 'Matrícula',
	brandModel: 'Marca y Modelo',
	visitor: 'Usuario',
	dates: 'Fecha',
	vehicleLicense: 'Matrícula'
};

export const getNameByKey = ( item ) => names[ item ] || item;

export const baseParams = { page: 1, pageSize: 10 };

export const initialPagination = {
	current: 1,
	pageSize: 10,
	total: null
};

export const initialFilters = {
	keyword: '',
	status: []
};

export const initialKPIFilters = {
	license: ''
};

export const layoutNewForm = {
	labelCol: {
		span: 6
	},
	wrapperCol: {
		span: 12
	}
};

export const localeText = { emptyText: NOT_RESULT_FOUND };

export const getIdFromPath = ( path = '' ) => path.split( '/' )[ 3 ];

export const getDataOptions = ( data ) => data.map( ( item ) => (
	{ key: item.id, name: item.description }
) );

export const genericRules = { required: true };

export const getColorByStatus = ( status ) => {
	switch ( status ) {
	case ACTIVE:
		return 'green';

	case INACTIVE:
		return 'volcano';

	case INVITED:
		return 'blue';

	default:
		return '';
	}
};

export const status = [
	{ key: ACTIVE_STATUS, name: ACTIVE },
	{ key: INVITED_STATUS, name: INVITED },
	{ key: INACTIVE_STATUS, name: INACTIVE }
];

export const optionsMap = {
	radius: 20,
	opacity: 0.6
};

export const initialZoom = 1;
export const initialCenter = { lat: 0, lng: 0 };
