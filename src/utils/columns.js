import React from 'react';
import { Popconfirm, Space, Tag } from 'antd';
import { DeleteFilled, EditFilled } from '@ant-design/icons';
import { getColorByStatus, getNameByKey } from './utils';
import theme from '../assets/theme';
import { ACTIVE } from './constants';

export const buildColumn = ( array ) => array.map( ( item ) => {
	if ( item === 'actions' ) {
		return {
			title: 'Acciones',
			key: 'action',
			render: ( record ) => (
				<Space size="middle">
					<EditFilled style={{ color: theme.secondaryColor }} onClick={record.onEdit} />
					{record.status === ACTIVE && (
						<Popconfirm
							title="¿Estás seguro que desea borrar?"
							onConfirm={record.onDelete}
							onCancel={() => {}}
							okText="Si"
							cancelText="No"
						>
							<DeleteFilled
								style={{ color: theme.secondaryColor }}
							/>
						</Popconfirm>
					)}
				</Space>

			)
		};
	}
	if ( item === 'status' ) {
		return {
			title: 'Estado',
			key: 'status',
			render: ( record ) => (
				<Tag color={getColorByStatus( record.status )} key={record.status}>
					{record.status}
				</Tag>
			)
		};
	}
	return {
		title: getNameByKey( item ),
		dataIndex: item
	};
} );

export const visitorColumn = buildColumn( [ 'firstName', 'lastName', 'email', 'idCard', 'vehicleLicense', 'status', 'actions' ] );
export const adminColumn = buildColumn( [ 'firstName', 'lastName', 'email', 'status', 'actions' ] );
export const vehicleColumn = buildColumn( [ 'type', 'license', 'brandModel', 'email', 'actions' ] );
