export const styles = {
	container: { minHeight: '100vh' },
	content: { display: 'flex', padding: '20px' },
	footer: {
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		textAlign: 'center'
	}
};
