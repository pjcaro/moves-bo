import styled from 'styled-components';

const Container = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	box-sizing: border-box;
	flex: 1;
`;

export default {
	Container
};
