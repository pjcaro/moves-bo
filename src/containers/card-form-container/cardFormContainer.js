import {
	Card, Col, Divider, Row
} from 'antd';
import React from 'react';
import { string, node } from 'prop-types';
import S from './styles';

const CardFormContainer = ( { title, children } ) => (
	<S.Container>
		<Row style={{ flex: 1 }} justify="center" align="middle">
			<Col flex={1} xs={{ span: 24 }} sm={{ span: 20 }} md={{ span: 16 }} lg={{ span: 12 }}>
				<Card>
					<Divider>{title}</Divider>
					{children}
				</Card>
			</Col>
		</Row>
	</S.Container>
);

CardFormContainer.propTypes = {
	title: string.isRequired,
	children: node.isRequired
};

export default CardFormContainer;
