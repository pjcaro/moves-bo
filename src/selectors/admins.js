import { createSelector } from 'reselect';

export const getAdminById = ( id ) => createSelector(
	( state ) => state.admins.admins,
	( admins ) => admins.find( ( admin ) => admin.id === id )
);
