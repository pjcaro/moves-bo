import { createSelector } from 'reselect';

export const getVehicleById = ( id ) => createSelector(
	( state ) => state.vehicles.vehicles,
	( vehicles ) => vehicles.find( ( vehicle ) => vehicle.id === id )
);
