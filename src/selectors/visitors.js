import { createSelector } from 'reselect';

export const getVisitorById = ( id ) => createSelector(
	( state ) => state.visitors.visitors,
	( visitors ) => visitors.find( ( visitor ) => visitor.id === id )
);
