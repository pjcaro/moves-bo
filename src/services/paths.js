// authentication
export const LOGIN_PATH = '/admin/user/login';
export const LOGOUT_PATH = '/admin/user/logout';
export const RECOVER_PASSWORD_PATH = '/admin/user/recover-password';
export const REFRESH_TOKEN_PATH = '/admin/user/refresh-session';

// visitors
export const GET_VISITORS_PATH = '/admin/visitor/list';
export const CREATE_VISITOR_PATH = '/admin/visitor/invite';
export const DELETE_VISITOR_PATH = '/admin/visitor/{id}';
export const EDIT_VISITOR_PATH = '/admin/visitor/{id}';

// user
export const GET_USER_PATH = '/admin/user/info';

// admin
export const GET_ADMINS_PATH = '/admin/user/list';
export const CREATE_ADMIN_PATH = '/admin/user/register';
export const DELETE_ADMIN_PATH = '/admin/user/{id}';
export const EDIT_ADMIN_PATH = '/admin/user/{id}';

// vehicles
export const GET_VEHICLES_PATH = '/admin/vehicle/list';
export const CREATE_VEHICLES_PATH = '/admin/vehicle/register';
export const EDIT_VEHICLES_PATH = '/admin/vehicle/{id}';
export const GET_VEHICLE_TYPES_PATH = '/admin/vehicle-type/list';
export const GET_BRAND_MODELS_PATH = '/admin/vehicle-type/{id}/brand-model/list';

// report
export const GET_REPORTS_PATH = '/admin/dashboard';
export const GET_REPORTS_FILTERED_PATH = '/admin/detailed-reports';

// contact-link
export const GET_CONTRACT_LINK_PATH = '/admin/contract-link';

// user-manual-link
export const GET_USER_MANUAL_LINK_PATH = '/admin/user-manual-link';
