/* eslint-disable import/no-cycle */
import axios from 'axios';
import { REFRESH_TOKEN_PATH } from './paths';
import { store } from '../store/store';
import { logout } from '../actions/authentication';

export const googleApiKey = 'AIzaSyB6o2H5ogsK8D7g_RVqB1ElJY1Z3uBRcZQ';

const baseURL = process.env.REACT_APP_API_URL;

export const API = axios.create();

API.defaults.baseURL = baseURL;

export const saveTokens = ( { accessToken, refreshToken } ) => {
	localStorage.setItem( 'accessToken', accessToken );
	localStorage.setItem( 'refreshToken', refreshToken );
};

export const removeTokens = () => {
	localStorage.removeItem( 'accessToken' );
	localStorage.removeItem( 'refreshToken' );
};

const renewToken = ( config ) => {
	const refreshToken = localStorage.getItem( 'refreshToken' );
	const request = {
		method: 'POST',
		baseURL,
		url: REFRESH_TOKEN_PATH,
		headers: {
			'Content-Type': 'application/json'
		},
		data: {
			refreshToken
		}
	};
	return axios( request )
		.then( ( response ) => {
			const { data: { data: { accessToken } } } = response;
			saveTokens( { accessToken, refreshToken } );
			return axios.request( {
				...config,
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `${accessToken || ''}`
				}
			} );
		} )
		.catch( () => {
			if ( refreshToken ) {
				store.dispatch( logout() );
			}
		} );
};

// Interceptor response
API.interceptors.response.use(
	( response ) => response,
	( error ) => {
		const { response: { status }, config } = error;
		if ( status === 401 ) {
			return renewToken( config );
		}
		return Promise.reject( error );
	}
);

// Interceptor request
API.interceptors.request.use( ( config ) => {
	const accessToken = localStorage.getItem( 'accessToken' );
	config.headers.Authorization = `${accessToken}`;
	return config;
} );
