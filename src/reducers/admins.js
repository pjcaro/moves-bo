import { handle } from 'redux-pack';
import { CREATE_ADMIN, EDIT_ADMIN, GET_ADMINS } from '../actions/types';
import { parserAdmin } from '../utils/parsers';
import { initialPagination } from '../utils/utils';

const initialState = {
	admins: [],
	pagination: initialPagination,
	isLoadingAdmins: false,
	isCreatingAdmin: false,
	isEditingAdmin: false
};

const adminsReducer = ( state = initialState, action ) => {
	switch ( action.type ) {
	case GET_ADMINS:
		return handle( state, action, {
			start: ( prevState ) => ( { ...prevState, isLoadingAdmins: true } ),
			success: ( prevState ) => ( {
				...prevState,
				isLoadingAdmins: false,
				admins: action?.payload?.data?.data.map( ( item ) => parserAdmin( item ) ),
				pagination: {
					...prevState.pagination,
					current: action?.payload?.data?.page,
					total: action?.payload?.data?.totalEntries
				}
			} ),
			failure: ( prevState ) => ( { ...prevState, isLoadingAdmins: false } )
		} );

	case CREATE_ADMIN:
		return handle( state, action, {
			start: ( prevState ) => ( { ...prevState, isCreatingAdmin: true } ),
			success: ( prevState ) => ( { ...prevState, isCreatingAdmin: false } ),
			failure: ( prevState ) => ( { ...prevState, isCreatingAdmin: false } )
		} );

	case EDIT_ADMIN:
		return handle( state, action, {
			start: ( prevState ) => ( { ...prevState, isEditingAdmin: true } ),
			success: ( prevState ) => ( { ...prevState, isEditingAdmin: false } ),
			failure: ( prevState ) => ( { ...prevState, isEditingAdmin: false } )
		} );

	default:
		return state;
	}
};

export default adminsReducer;
