import { handle } from 'redux-pack';
import { CREATE_VISITOR, EDIT_VISITOR, GET_VISITORS } from '../actions/types';
import { parserVisitor } from '../utils/parsers';
import { initialPagination } from '../utils/utils';

const initialState = {
	visitors: [],
	pagination: initialPagination,
	isLoadingVisitors: false,
	isCreatingVisitor: false,
	isEditingVisitor: false
};

const visitorsReducer = ( state = initialState, action ) => {
	switch ( action.type ) {
	case GET_VISITORS:
		return handle( state, action, {
			start: ( prevState ) => ( { ...prevState, isLoadingVisitors: true } ),
			success: ( prevState ) => ( {
				...prevState,
				isLoadingVisitors: false,
				visitors: action?.payload?.data?.data?.map( ( item ) => parserVisitor( item ) ),
				pagination: {
					...prevState.pagination,
					current: action?.payload?.data?.page,
					total: action?.payload?.data?.totalEntries
				}
			} ),
			failure: ( prevState ) => ( { ...prevState, isLoadingVisitors: false } )
		} );

	case CREATE_VISITOR:
		return handle( state, action, {
			start: ( prevState ) => ( { ...prevState, isCreatingVisitor: true } ),
			success: ( prevState ) => ( { ...prevState, isCreatingVisitor: false } ),
			failure: ( prevState ) => ( { ...prevState, isCreatingVisitor: false } )
		} );

	case EDIT_VISITOR:
		return handle( state, action, {
			start: ( prevState ) => ( { ...prevState, isEditingVisitor: true } ),
			success: ( prevState ) => ( { ...prevState, isEditingVisitor: false } ),
			failure: ( prevState ) => ( { ...prevState, isEditingVisitor: false } )
		} );

	default:
		return state;
	}
};

export default visitorsReducer;
