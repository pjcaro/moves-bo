import { combineReducers } from 'redux';
import userReducer from './user';
import authenticationReducer from './authentication';
import { LOGOUT } from '../actions/types';
import visitorsReducer from './visitors';
import adminsReducer from './admins';
import vehiclesReducer from './vehicles';
import reportsReducer from './reports';

export const appReducer = combineReducers( {
	user: userReducer,
	authentication: authenticationReducer,
	visitors: visitorsReducer,
	admins: adminsReducer,
	vehicles: vehiclesReducer,
	reports: reportsReducer
} );

export const rootReducer = ( state, action ) => {
	if ( action.type === LOGOUT ) {
		state = undefined;
	}
	return appReducer( state, action );
};
