import { handle } from 'redux-pack';
import { GET_USER, SET_TOKENS } from '../actions/types';
import { initializeUser } from '../utils/initializer';
import { parserUser } from '../utils/parsers';

const initialState = {
	accessToken: null,
	refreshToken: null,
	user: initializeUser()
};

const userReducer = ( state = initialState, action ) => {
	switch ( action.type ) {
	case SET_TOKENS:
		return {
			...state,
			accessToken: action.accessToken,
			refreshToken: action.refreshToken
		};

	case GET_USER:
		return handle( state, action, {
			success: ( prevState ) => ( {
				...prevState,
				user: parserUser( action?.payload?.data?.data )
			} )
		} );

	default:
		return state;
	}
};

export default userReducer;
