import { handle } from 'redux-pack';
import {
	GET_CONTRACT_LINK, LOGIN, RECOVER_PASSWORD, GET_USER_MANUAL_LINK
} from '../actions/types';

const initialState = {
	isLogging: false,
	idLoadingRecoverPassword: false,
	isLogged: false,
	contractLink: null,
	userManualLink: null
};

const authenticationReducer = ( state = initialState, action ) => {
	switch ( action.type ) {
	case LOGIN:
		return handle( state, action, {
			start: ( prevState ) => ( { ...prevState, isLogging: true } ),
			success: ( prevState ) => ( { ...prevState, isLogging: false, isLogged: true } ),
			failure: ( prevState ) => ( { ...prevState, isLogging: false } )
		} );
	case RECOVER_PASSWORD:
		return handle( state, action, {
			start: ( prevState ) => ( { ...prevState, idLoadingRecoverPassword: true } ),
			success: ( prevState ) => ( { ...prevState, idLoadingRecoverPassword: false } ),
			failure: ( prevState ) => ( { ...prevState, idLoadingRecoverPassword: false } )
		} );

	case GET_CONTRACT_LINK:
		return handle( state, action, {
			success: ( prevState ) => ( { ...prevState, contractLink: action?.payload?.data?.data } )
		} );

	case GET_USER_MANUAL_LINK:
		return handle( state, action, {
			success: ( prevState ) => ( { ...prevState, userManualLink: action?.payload?.data?.data } )
		} );

	default:
		return state;
	}
};

export default authenticationReducer;
