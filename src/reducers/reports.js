import { handle } from 'redux-pack';
import { GET_REPORTS, GET_REPORTS_FILTERED, SEARCH_VEHICLES } from '../actions/types';
import { initializeReport, initilizeReportHeatMap } from '../utils/initializer';
import { parserReports, parserReportsHeatMap } from '../utils/parsers';

export const initialState = {
	vehiclesResult: [],
	isSearchingVehicles: false,
	isLoadingReports: false,
	isLoadingReportsFiltered: false,
	emission: initializeReport(),
	energeticConsumption: initializeReport(),
	operativeCosts: initializeReport(),
	areasCovered: initilizeReportHeatMap(),
	hoursOfUse: initializeReport(),
	avoidedEmission: initializeReport(),
	energeticConsumptionFiltered: initializeReport(),
	expenses: initializeReport(),
	loadDistribution: initializeReport(),
	kilometers: initilizeReportHeatMap(),
	travelEvents: initializeReport(),
	chargeTime: initializeReport(),
	gForceEvents: initializeReport()
};

const reportsReducer = ( state = initialState, action ) => {
	switch ( action.type ) {
	case SEARCH_VEHICLES:
		return handle( state, action, {
			start: ( prevState ) => ( { ...prevState, isSearchingVehicles: true } ),
			success: ( prevState ) => ( {
				...prevState, isSearchingVehicles: false, vehiclesResult: []
			} ),
			failure: ( prevState ) => ( { ...prevState, isSearchingVehicles: false } )
		} );

	case GET_REPORTS:
		return handle( state, action, {
			start: ( prevState ) => ( { ...prevState, isLoadingReports: true } ),
			success: ( prevState ) => ( {
				...prevState,
				isLoadingReports: false,
				emission: parserReports( action?.payload?.data?.data?.emission ),
				energeticConsumption: parserReports( action?.payload?.data?.data?.energeticConsumption ),
				operativeCosts: parserReports( action?.payload?.data?.data?.operativeCosts )

			} )
		} );

	case GET_REPORTS_FILTERED:
		return handle( state, action, {
			start: ( prevState ) => ( { ...prevState, isLoadingReportsFiltered: true } ),
			success: ( prevState ) => ( {
				...prevState,
				isLoadingReportsFiltered: false,
				hoursOfUse: parserReports( action?.payload?.data?.data?.vehiclesUsage ),
				energeticConsumptionFiltered: parserReports(
					action?.payload?.data?.data?.energeticConsumption
				),
				loadDistribution: parserReports( action?.payload?.data?.data?.chargeDistribution ),
				avoidedEmission: parserReports( action?.payload?.data?.data?.avoidedEmission ),
				expenses: parserReports( action?.payload?.data?.data?.operativeCosts ),
				areasCovered: parserReportsHeatMap( action?.payload?.data?.data?.heatMap ),
				kilometers: parserReports( action?.payload?.data?.data?.kilometers ),
				travelEvents: parserReports( action?.payload?.data?.data?.travelEvents ),
				chargeTime: parserReports( action?.payload?.data?.data?.chargeTime ),
				gForceEvents: parserReports( action?.payload?.data?.data?.gForceEvents )
			} ),
			failure: ( prevState ) => ( { ...prevState, isLoadingReportsFiltered: false } )
		} );

	default:
		return state;
	}
};

export default reportsReducer;
