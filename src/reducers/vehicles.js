import { handle } from 'redux-pack';
import {
	CREATE_VEHICLE, EDIT_VEHICLE, GET_BRAND_MODEL, GET_VEHICLES, GET_VEHICLE_TYPES, SEARCH_VISITOR
} from '../actions/types';
import {
	parserBrandModel, parserVehicle, parserVehicletypes, parserVisitor
} from '../utils/parsers';
import { initialPagination } from '../utils/utils';

const initialState = {
	vehicles: [],
	types: [],
	brandModels: [],
	pagination: initialPagination,
	isLoadingVehicles: false,
	isCreatingVehicle: false,
	isEditingVehicle: false,
	isLoadingTypes: false,
	isLoadingBrandModel: false,
	isSearchingVisitor: false,
	visitorsResult: []
};

const vehiclesReducer = ( state = initialState, action ) => {
	switch ( action.type ) {
	case GET_VEHICLES:
		return handle( state, action, {
			start: ( prevState ) => ( { ...prevState, isLoadingVehicles: true } ),
			success: ( prevState ) => ( {
				...prevState,
				isLoadingVehicles: false,
				vehicles: action?.payload?.data?.data.map( ( item ) => parserVehicle( item ) ),
				pagination: {
					...prevState.pagination,
					current: action?.payload?.data?.page,
					total: action?.payload?.data?.totalEntries
				}
			} ),
			failure: ( prevState ) => ( { ...prevState, isLoadingVehicles: false } )
		} );

	case CREATE_VEHICLE:
		return handle( state, action, {
			start: ( prevState ) => ( { ...prevState, isCreatingVehicle: true } ),
			success: ( prevState ) => ( { ...prevState, isCreatingVehicle: false } ),
			failure: ( prevState ) => ( { ...prevState, isCreatingVehicle: false } )
		} );

	case EDIT_VEHICLE:
		return handle( state, action, {
			start: ( prevState ) => ( { ...prevState, isEditingVehicle: true } ),
			success: ( prevState ) => ( { ...prevState, isEditingVehicle: false } ),
			failure: ( prevState ) => ( { ...prevState, isEditingVehicle: false } )
		} );

	case SEARCH_VISITOR:
		return handle( state, action, {
			start: ( prevState ) => ( { ...prevState, isSearchingVisitor: true } ),
			success: ( prevState ) => ( {
				...prevState,
				isSearchingVisitor: false,
				visitorsResult: action?.payload?.data?.data.map( ( item ) => parserVisitor( item ) )
			} ),
			failure: ( prevState ) => ( { ...prevState, isSearchingVisitor: false } )
		} );

	case GET_VEHICLE_TYPES:
		return handle( state, action, {
			start: ( prevState ) => ( { ...prevState, isLoadingTypes: true } ),
			success: ( prevState ) => ( {
				...prevState,
				isLoadingTypes: false,
				types: action?.payload?.data?.data.map( ( item ) => parserVehicletypes( item ) )
			} )
		} );

	case GET_BRAND_MODEL:
		return handle( state, action, {
			start: ( prevState ) => ( { ...prevState, isLoadingBrandModel: true } ),
			success: ( prevState ) => ( {
				...prevState,
				isLoadingBrandModel: false,
				brandModels: action?.payload?.data?.data.map( ( item ) => parserBrandModel( item ) )
			} ),
			failure: ( prevState ) => ( { ...prevState, isLoadingBrandModel: false } )
		} );

	default:
		return state;
	}
};

export default vehiclesReducer;
