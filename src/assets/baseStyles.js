const baseStyles = {
	padding: '20px'
};

const baseButtonStyles = {
	position: 'absolute', right: '20px', top: '20px'
};

const baseFormStyles = {
	formItem: {
		display: 'flex',
		justifyContent: 'center'
	},
	buttonsContainer: {
		display: 'flex',
		justifyContent: 'flex-end',
		width: '100%'
	},
	button: {
		marginRight: '10px'
	}
};

export {
	baseStyles,
	baseButtonStyles,
	baseFormStyles
};
