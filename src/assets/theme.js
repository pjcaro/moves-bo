const theme = {
	primaryColor: '#032465',
	secondaryColor: '#4674AA',
	backgroundColor: '#f5f5f5',
	green: '#7cb305',
	yellow: '#fec514',
	orange: '#d4380d',
	blue: '#1890ff',
	whiteOpacity: 'rgba(255, 255, 255, 0.65)'
};

export default theme;
