import { API } from '../services/API';
import { GET_REPORTS_FILTERED_PATH, GET_REPORTS_PATH, GET_VEHICLES_PATH } from '../services/paths';
import { ERROR } from '../utils/constants';
import { showMessage } from '../utils/messageUtils';
import { GET_REPORTS, GET_REPORTS_FILTERED, SEARCH_VEHICLES } from './types';

const searchVechiles = ( { keyword = '' } ) => ( dispatch ) => dispatch( {
	type: SEARCH_VEHICLES,
	promise: API.get( GET_VEHICLES_PATH, { params: { keyword } } ),
	meta: {
		onFailure: ( error ) => showMessage( { type: ERROR, msg: error?.response?.data?.error } )
	}
} );

const getReports = () => ( dispatch ) => dispatch( {
	type: GET_REPORTS,
	promise: API.get( GET_REPORTS_PATH ),
	meta: {
		onFailure: ( error ) => showMessage( { type: ERROR, msg: error?.response?.data?.error } )
	}
} );

const getReportsFiltered = ( { vehicles, from, to } = {} ) => ( dispatch ) => dispatch( {
	type: GET_REPORTS_FILTERED,
	promise: API.get( GET_REPORTS_FILTERED_PATH, { params: { vehicles, from, to } } ),
	meta: {
		onFailure: ( error ) => showMessage( { type: ERROR, msg: error?.response?.data?.error } )
	}
} );

export {
	searchVechiles,
	getReports,
	getReportsFiltered
};
