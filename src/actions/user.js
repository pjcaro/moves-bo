import { API } from '../services/API';
import { GET_USER_PATH } from '../services/paths';
import { ERROR } from '../utils/constants';
import { showMessage } from '../utils/messageUtils';
import { GET_USER, SET_TOKENS } from './types';

const setTokens = ( { accessToken, refreshToken } ) => ( dispatch ) => dispatch( {
	type: SET_TOKENS,
	accessToken,
	refreshToken
} );

const getUser = () => ( dispatch ) => dispatch( {
	type: GET_USER,
	promise: API.get( GET_USER_PATH ),
	meta: {
		onFailure: ( error ) => showMessage( { type: ERROR, msg: error?.response?.data?.error } )
	}
} );

export {
	setTokens,
	getUser
};
