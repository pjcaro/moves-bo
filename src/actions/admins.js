import { API } from '../services/API';
import {
	CREATE_ADMIN_PATH, DELETE_ADMIN_PATH, EDIT_ADMIN_PATH, GET_ADMINS_PATH
} from '../services/paths';
import { ERROR, SUCCESS } from '../utils/constants';
import { showMessage } from '../utils/messageUtils';
import {
	CREATE_ADMIN, DELETE_ADMIN, EDIT_ADMIN, GET_ADMINS
} from './types';

const getAdmins = ( params = {} ) => ( dispatch ) => dispatch( {
	type: GET_ADMINS,
	promise: API.get( GET_ADMINS_PATH, { params } ),
	meta: {
		onFailure: ( error ) => showMessage( { type: ERROR, msg: error?.response?.data?.error } )
	}
} );

const createAdmin = ( form, callback ) => ( dispatch ) => dispatch( {
	type: CREATE_ADMIN,
	promise: API.post( CREATE_ADMIN_PATH, form ),
	meta: {
		onSuccess: ( res ) => {
			callback();
			showMessage( { type: SUCCESS, msg: res?.data.message } );
		},
		onFailure: ( error ) => showMessage( { type: ERROR, msg: error?.response?.data?.error } )
	}
} );

const editAdmin = ( id, form, callback = () => {} ) => ( dispatch ) => dispatch( {
	type: EDIT_ADMIN,
	promise: API.patch( EDIT_ADMIN_PATH.replace( '{id}', id ), form ),
	meta: {
		onSuccess: ( res ) => {
			callback();
			showMessage( { type: SUCCESS, msg: res?.data.message } );
		},
		onFailure: ( error ) => showMessage( { type: ERROR, msg: error?.response?.data?.error } )
	}
} );

const deleteAdmin = ( id ) => ( dispatch ) => dispatch( {
	type: DELETE_ADMIN,
	promise: API.delete( DELETE_ADMIN_PATH.replace( '{id}', id ) ),
	meta: {
		onSuccess: ( response ) => {
			dispatch( getAdmins() );
			showMessage( { type: SUCCESS, msg: response?.data?.message } );
		},
		onFailure: ( error ) => showMessage( { type: ERROR, msg: error?.response?.data?.error } )
	}
} );

export {
	getAdmins,
	createAdmin,
	deleteAdmin,
	editAdmin
};
