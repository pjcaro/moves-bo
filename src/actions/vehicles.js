import { API } from '../services/API';
import {
	CREATE_VEHICLES_PATH,
	EDIT_VEHICLES_PATH,
	GET_BRAND_MODELS_PATH,
	GET_VEHICLES_PATH,
	GET_VEHICLE_TYPES_PATH,
	GET_VISITORS_PATH
} from '../services/paths';
import {
	ACTIVE_STATUS, ERROR, INVITED_STATUS, SUCCESS
} from '../utils/constants';
import { showMessage } from '../utils/messageUtils';
import {
	CREATE_VEHICLE, EDIT_VEHICLE, GET_BRAND_MODEL, GET_VEHICLES, GET_VEHICLE_TYPES, SEARCH_VISITOR
} from './types';

const getVehicles = ( params = {} ) => ( dispatch ) => dispatch( {
	type: GET_VEHICLES,
	promise: API.get( GET_VEHICLES_PATH, { params } ),
	meta: {
		onFailure: ( error ) => showMessage( { type: ERROR, msg: error?.response?.data?.error } )
	}
} );

const createVehicle = ( form, callback ) => ( dispatch ) => dispatch( {
	type: CREATE_VEHICLE,
	promise: API.post( CREATE_VEHICLES_PATH, form ),
	meta: {
		onSuccess: ( res ) => {
			callback();
			showMessage( { type: SUCCESS, msg: res?.data.message } );
		},
		onFailure: ( error ) => showMessage( { type: ERROR, msg: error?.response?.data?.error } )
	}
} );

const edtiVehicle = ( id, form, callback = () => {} ) => ( dispatch ) => dispatch( {
	type: EDIT_VEHICLE,
	promise: API.patch( EDIT_VEHICLES_PATH.replace( '{id}', id ), form ),
	meta: {
		onSuccess: ( res ) => {
			callback();
			showMessage( { type: SUCCESS, msg: res?.data.message } );
		},
		onFailure: ( error ) => showMessage( { type: ERROR, msg: error?.response?.data?.error } )
	}
} );

const searchVisitor = (
	keyword = '',
	status = [ ACTIVE_STATUS, INVITED_STATUS ],
	hasVehicle = false
) => ( dispatch ) => dispatch( {
	type: SEARCH_VISITOR,
	promise: API.get( GET_VISITORS_PATH, { params: { keyword, status, hasVehicle } } ),
	meta: {
		onFailure: ( error ) => showMessage( { type: ERROR, msg: error?.response?.data?.error } )
	}
} );

const getVehicleTypes = () => ( dispatch ) => dispatch( {
	type: GET_VEHICLE_TYPES,
	promise: API.get( GET_VEHICLE_TYPES_PATH ),
	meta: {
		onFailure: ( error ) => showMessage( { type: ERROR, msg: error?.response?.data?.error } )
	}
} );

const getBrandModels = ( id ) => ( dispatch ) => dispatch( {
	type: GET_BRAND_MODEL,
	promise: API.get( GET_BRAND_MODELS_PATH.replace( '{id}', id ) ),
	meta: {
		onFailure: ( error ) => showMessage( { type: ERROR, msg: error?.response?.data?.error } )
	}
} );

export {
	getVehicles,
	createVehicle,
	edtiVehicle,
	searchVisitor,
	getVehicleTypes,
	getBrandModels
};
