// authentication
export const LOGIN = '@Authentication/LOGIN';
export const LOGOUT = '@Authentication/LOGOUT';
export const RECOVER_PASSWORD = '@Authentication/RECOVER_PASSWORD';
export const GET_CONTRACT_LINK = '@Authentication/GET_CONTRACT_LINK';
export const GET_USER_MANUAL_LINK = '@Authentication/GET_USER_MANUAL_LINK';

// user
export const SET_TOKENS = '@User/SET_TOKENS';
export const GET_USER = '@User/GET_USER';

// visitors
export const GET_VISITORS = '@Visitors/GET_VISITORS';
export const CREATE_VISITOR = '@Visitors/CREATE_VISITOR';
export const DELETE_VISITOR = '@Visitor/DELETE_VISITOR';
export const EDIT_VISITOR = '@Visitor/EDIT_VISITOR';

// admins
export const GET_ADMINS = '@Admins/GET_ADMINS';
export const CREATE_ADMIN = '@Admins/CREATE_ADMIN';
export const DELETE_ADMIN = '@Admins/DELETE_ADMIN';
export const EDIT_ADMIN = '@Admins/EDIT_ADMIN';

// vehicles
export const GET_VEHICLES = '@Vehicles/GET_VEHICLES';
export const CREATE_VEHICLE = '@Vehicles/CREATE_VEHICLE';
export const DELETE_VEHICLE = '@Vehicles/DELETE_VEHICLE';
export const EDIT_VEHICLE = '@Vehicles/EDIT_VEHICLE';
export const SEARCH_VISITOR = '@Vehicles/SEARCH_VISITOR';
export const GET_VEHICLE_TYPES = '@Vehicles/GET_VEHICLE_TYPES';
export const GET_BRAND_MODEL = '@Vehicles/GET_BRAND_MODEL';

// reports
export const SEARCH_VEHICLES = '@Reports/SEARCH_VEHICLES';
export const GET_REPORTS = '@Reports/GET_REPORTS';
export const GET_REPORTS_FILTERED = '@Reports/GET_REPORTS_FILTERED';
