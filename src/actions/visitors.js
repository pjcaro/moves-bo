import { API } from '../services/API';
import {
	CREATE_VISITOR_PATH, DELETE_VISITOR_PATH, EDIT_VISITOR_PATH, GET_VISITORS_PATH
} from '../services/paths';
import {
	CREATE_VISITOR, DELETE_VISITOR, EDIT_VISITOR, GET_VISITORS
} from './types';
import { showMessage } from '../utils/messageUtils';
import { ERROR, SUCCESS } from '../utils/constants';

const getVisitors = ( params = {} ) => ( dispatch ) => dispatch( {
	type: GET_VISITORS,
	promise: API.get( GET_VISITORS_PATH, { params } ),
	meta: {
		onFailure: ( error ) => showMessage( { type: ERROR, msg: error?.response?.data?.error } )
	}
} );

const createVisitor = ( { email }, callback = () => {} ) => ( dispatch ) => dispatch( {
	type: CREATE_VISITOR,
	promise: API.post( CREATE_VISITOR_PATH, { email } ),
	meta: {
		onSuccess: ( res ) => {
			callback();
			showMessage( { type: SUCCESS, msg: res?.data.message } );
		},
		onFailure: ( error ) => showMessage( { type: ERROR, msg: error?.response?.data?.error } )
	}
} );

const editVisitor = ( id, form, callback = () => {} ) => ( dispatch ) => dispatch( {
	type: EDIT_VISITOR,
	promise: API.patch( EDIT_VISITOR_PATH.replace( '{id}', id ), form ),
	meta: {
		onSuccess: ( res ) => {
			callback();
			showMessage( { type: SUCCESS, msg: res?.data.message } );
		},
		onFailure: ( error ) => showMessage( { type: ERROR, msg: error?.response?.data?.error } )
	}
} );

const deleteVisitor = ( id ) => ( dispatch ) => dispatch( {
	type: DELETE_VISITOR,
	promise: API.delete( DELETE_VISITOR_PATH.replace( '{id}', id ) ),
	meta: {
		onSuccess: ( response ) => {
			dispatch( getVisitors() );
			showMessage( { type: SUCCESS, msg: response?.data?.message } );
		},
		onFailure: ( error ) => showMessage( { type: ERROR, msg: error?.response?.data?.error } )
	}
} );

export {
	getVisitors,
	createVisitor,
	deleteVisitor,
	editVisitor
};
