/* eslint-disable import/no-cycle */
import {
	GET_CONTRACT_LINK, GET_USER_MANUAL_LINK, LOGIN, LOGOUT, RECOVER_PASSWORD
} from './types';
import { API, removeTokens, saveTokens } from '../services/API';
import {
	GET_CONTRACT_LINK_PATH, GET_USER_MANUAL_LINK_PATH, LOGIN_PATH, LOGOUT_PATH, RECOVER_PASSWORD_PATH
} from '../services/paths';
import { getUser } from './user';
import { showMessage } from '../utils/messageUtils';
import { ERROR, SUCCESS } from '../utils/constants';

const login = ( { email, password }, callback ) => ( dispatch ) => dispatch( {
	type: LOGIN,
	promise: API.post( LOGIN_PATH, { email, password } ),
	meta: {
		onSuccess: ( response ) => {
			const { data: { data: { accessToken, refreshToken } } } = response;
			saveTokens( { accessToken, refreshToken } );
			dispatch( getUser() );
			callback();
		},
		onFailure: ( error ) => showMessage( { type: ERROR, msg: error?.response?.data?.error } )
	}
} );

const logout = () => ( dispatch ) => dispatch( {
	type: LOGOUT,
	promise: API.post( LOGOUT_PATH ),
	meta: {
		onSuccess: () => removeTokens()
	}
} );

const recoverPassword = ( { email }, callback ) => ( dispatch ) => dispatch( {
	type: RECOVER_PASSWORD,
	promise: API.post( RECOVER_PASSWORD_PATH, { email } ),
	meta: {
		onSuccess: ( res ) => {
			callback();
			showMessage( { type: SUCCESS, msg: res?.data.message } );
		},
		onFailure: ( error ) => showMessage( { type: ERROR, msg: error?.response?.data?.error } )
	}
} );

const getContractLink = () => ( dispatch ) => dispatch( {
	type: GET_CONTRACT_LINK,
	promise: API.get( GET_CONTRACT_LINK_PATH ),
	meta: {
		onFailure: ( error ) => showMessage( { type: ERROR, msg: error?.response?.data?.error } )
	}
} );

const getUserManualLink = () => ( dispatch ) => dispatch( {
	type: GET_USER_MANUAL_LINK,
	promise: API.get( GET_USER_MANUAL_LINK_PATH ),
	meta: {
		onFailure: ( error ) => showMessage( { type: ERROR, msg: error?.response?.data?.error } )
	}
} );

export {
	login,
	logout,
	recoverPassword,
	getContractLink,
	getUserManualLink
};
