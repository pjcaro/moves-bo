/* eslint-disable react/jsx-props-no-spreading */
import React, { useEffect } from 'react';
import { bool, func } from 'prop-types';
import {
	Form, Input
} from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import debounce from 'lodash.debounce';
import { validateMessages } from '../../utils/validateMessages';
import {
	genericRules, getDataOptions, getNameByKey, layoutNewForm
} from '../../utils/utils';
import { Dropdown, FormButtons } from '../../components';
import { getBrandModels, getVehicleTypes, searchVisitor } from '../../actions/vehicles';

const NewVehicleForm = ( { onSubmit, onCancel, isLoading } ) => {
	const [ form ] = Form.useForm();
	const types = useSelector( ( state ) => state.vehicles.types );
	const brandModels = useSelector( ( state ) => state.vehicles.brandModels );
	const visitorsResult = useSelector( ( state ) => state.vehicles.visitorsResult );
	const isLoadingTypes = useSelector( ( state ) => state.vehicles.isLoadingTypes );
	const isLoadingBrandModel = useSelector( ( state ) => state.vehicles.isLoadingBrandModel );
	const isSearchingVisitor = useSelector( ( state ) => state.vehicles.isSearchingVisitor );
	const dispatch = useDispatch();

	useEffect( () => {
		dispatch( getVehicleTypes() );
		dispatch( searchVisitor() );
	}, [] );

	const handleSelectType = ( idType ) => {
		form.setFieldsValue( { brandModelId: null } );
		dispatch( getBrandModels( idType ) );
	};

	const handleSearchVisitor = ( keyword ) => dispatch( searchVisitor( keyword ) );

	return (
		<Form
			form={form}
			{...layoutNewForm}
			onFinish={onSubmit}
			validateMessages={validateMessages}
			name="control-hooks"
		>
			<Form.Item
				name={[ 'license' ]}
				label={getNameByKey( 'license' )}
				rules={[ genericRules ]}
			>
				<Input />
			</Form.Item>
			<Form.Item
				name={[ 'typeId' ]}
				label={getNameByKey( 'type' )}
				rules={[ genericRules ]}
			>
				{Dropdown( {
					disabled: isLoadingTypes,
					loading: isLoadingTypes,
					data: getDataOptions( types ),
					placeholder: 'Seleccione un tipo',
					onSelect: handleSelectType
				} )}
			</Form.Item>
			<Form.Item
				name={[ 'brandModelId' ]}
				label={getNameByKey( 'brandModel' )}
				rules={[ genericRules ]}
			>
				{Dropdown( {
					disabled: !brandModels.length,
					loading: isLoadingBrandModel,
					data: getDataOptions( brandModels ),
					placeholder: 'Seleccione marca - modelo'
				} )}
			</Form.Item>
			<Form.Item
				name={[ 'visitorId' ]}
				label={getNameByKey( 'visitor' )}
				rules={[ genericRules ]}
			>
				{Dropdown( {
					loading: isSearchingVisitor,
					withSearch: true,
					data: visitorsResult.map( ( visitor ) => ( { key: visitor.id, name: visitor.email } ) ),
					placeholder: 'Seleccione un usuario',
					onSearch: debounce( handleSearchVisitor, 500 )
				} )}
			</Form.Item>
			<FormButtons isLoading={isLoading} onSubmit={onSubmit} onCancel={onCancel} />
		</Form>
	);
};

NewVehicleForm.propTypes = {
	onSubmit: func.isRequired,
	onCancel: func.isRequired,
	isLoading: bool
};

NewVehicleForm.defaultProps = {
	isLoading: false
};

export default NewVehicleForm;
