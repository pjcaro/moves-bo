import React from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import CardFormContainer from '../../containers/card-form-container/cardFormContainer';
import NewVehicleForm from './newVehicleForm';
import { createVehicle } from '../../actions/vehicles';
import { VEHICLES_ROUTE } from '../../routing/paths';

const NewAdminScreen = () => {
	const dispatch = useDispatch();
	const history = useHistory();
	const isCreatingVehicle = useSelector( ( state ) => state.vehicles.isCreatingVehicle );

	const handleSubmit = ( form ) => {
		dispatch( createVehicle( form, () => history.push( VEHICLES_ROUTE ) ) );
	};

	const handleCancel = () => history.goBack();

	return (
		<CardFormContainer title="Nuevo vehiculo">
			<NewVehicleForm
				onSubmit={handleSubmit}
				onCancel={handleCancel}
				isLoading={isCreatingVehicle}
			/>
		</CardFormContainer>
	);
};

export default NewAdminScreen;
