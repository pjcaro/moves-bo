import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router';
import { editVisitor } from '../../actions/visitors';
import CardFormContainer from '../../containers/card-form-container/cardFormContainer';
import { VISITORS_ROUTE } from '../../routing/paths';
import { getVisitorById } from '../../selectors/visitors';
import { getIdFromPath } from '../../utils/utils';
import EditVisitorForm from './editVisitorForm';

const EditVisitorScreen = () => {
	const isEditingVisitor = useSelector( ( state ) => state.visitors.isEditingVisitor );
	const location = useLocation();
	const id = getIdFromPath( location.pathname );
	const visitor = useSelector( getVisitorById( id ) );
	const dispatch = useDispatch();
	const history = useHistory();

	const handleSubmit = ( form ) => (
		dispatch( editVisitor( id, form, () => history.push( VISITORS_ROUTE ) ) )
	);

	const handleCancel = () => history.goBack();

	return (
		<CardFormContainer title="Editar usuario">
			<EditVisitorForm
				form={visitor}
				onSubmit={handleSubmit}
				onCancel={handleCancel}
				isLoading={isEditingVisitor}
			/>
		</CardFormContainer>
	);
};

export default EditVisitorScreen;
