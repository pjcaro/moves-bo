/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { bool, func, shape } from 'prop-types';
import {
	Form, Input
} from 'antd';
import { validateMessages } from '../../utils/validateMessages';
import { layoutNewForm } from '../../utils/utils';
import { Visitor } from '../../entity';
import { FormButtons } from '../../components';

const EditVisitorForm = ( {
	form, onSubmit, onCancel, isLoading
} ) => (
	<Form
		initialValues={form}
		{...layoutNewForm}
		onFinish={onSubmit}
		validateMessages={validateMessages}
		name="nest-messages"
	>
		<Form.Item
			name={[ 'firstName' ]}
			label="Nombre"
			rules={[
				{
					required: true
				}
			]}
		>
			<Input value={form.firstName} />
		</Form.Item>
		<Form.Item
			name={[ 'lastName' ]}
			label="Apellido"
			rules={[
				{
					required: true
				}
			]}
		>
			<Input value={form.lastName} />
		</Form.Item>
		<Form.Item
			name={[ 'idCard' ]}
			label="Cédula"
			rules={[
				{
					required: true
				}
			]}
		>
			<Input value={form.idCard} />
		</Form.Item>
		<FormButtons isLoading={isLoading} onCancel={onCancel} />
	</Form>
);

EditVisitorForm.propTypes = {
	form: shape( Visitor ).isRequired,
	onSubmit: func.isRequired,
	onCancel: func.isRequired,
	isLoading: bool
};

EditVisitorForm.defaultProps = {
	isLoading: false
};

export default EditVisitorForm;
