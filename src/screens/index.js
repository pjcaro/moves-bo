import LoginScreen from './login';
import PageNotFound from './404';
import VisitorsScreen from './visitors';
import ResetPasswordScreen from './reset-password';
import NewVisitorScreen from './new-visitor';
import AdminsScreen from './admin';
import NewAdminScreen from './new-admin';
import EditVisitorScreen from './edit-visitor';
import VehiclesScreen from './vehicles';
import NewVehicleScreen from './new-vehicle';
import DashboardScreen from './dashboard';

export {
	LoginScreen,
	PageNotFound,
	VisitorsScreen,
	ResetPasswordScreen,
	NewVisitorScreen,
	AdminsScreen,
	NewAdminScreen,
	EditVisitorScreen,
	VehiclesScreen,
	NewVehicleScreen,
	DashboardScreen
};
