/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { bool, func } from 'prop-types';
import {
	Form, Input
} from 'antd';
import { layoutNewForm } from '../../utils/utils';
import { validateMessages } from '../../utils/validateMessages';
import { FormButtons } from '../../components';

const NewAdminForm = ( { onSubmit, onCancel, isLoading } ) => (
	<Form {...layoutNewForm} onFinish={onSubmit} validateMessages={validateMessages} name="nest-messages">
		<Form.Item
			name={[ 'firstName' ]}
			label="Nombre"
			rules={[
				{
					required: true
				}
			]}
		>
			<Input />
		</Form.Item>
		<Form.Item
			name={[ 'lastName' ]}
			label="Apellido"
			rules={[
				{
					required: true
				}
			]}
		>
			<Input />
		</Form.Item>
		<Form.Item
			name={[ 'email' ]}
			label="Email"
			rules={[
				{
					required: true,
					type: 'email'
				}
			]}
		>
			<Input />
		</Form.Item>
		<FormButtons isLoading={isLoading} onCancel={onCancel} />
	</Form>
);

NewAdminForm.propTypes = {
	onSubmit: func.isRequired,
	onCancel: func.isRequired,
	isLoading: bool
};

NewAdminForm.defaultProps = {
	isLoading: false
};

export default NewAdminForm;
