import React from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import CardFormContainer from '../../containers/card-form-container/cardFormContainer';
import NewAdminForm from './newAdminForm';
import { createAdmin } from '../../actions/admins';

const NewAdminScreen = () => {
	const dispatch = useDispatch();
	const history = useHistory();
	const isCreatingAdmin = useSelector( ( state ) => state.admins.isCreatingAdmin );

	const handleSubmit = ( form ) => dispatch( createAdmin( form, () => history.push( '/admins' ) ) );

	const handleCancel = () => history.goBack();

	return (
		<CardFormContainer title="Nuevo administrador">
			<NewAdminForm
				onSubmit={handleSubmit}
				onCancel={handleCancel}
				isLoading={isCreatingAdmin}
			/>
		</CardFormContainer>
	);
};

export default NewAdminScreen;
