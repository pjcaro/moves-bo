import React from 'react';
import {
	Divider, Row
} from 'antd';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import ResetPasswordForm from './resetPasswordForm';
import { recoverPassword } from '../../actions/authentication';
import CardFormContainer from '../../containers/card-form-container/cardFormContainer';

const ResetPasswordScreen = () => {
	const dispatch = useDispatch();
	const history = useHistory();
	const isLogging = useSelector( ( state ) => state.authentication.idLoadingRecoverPassword );
	const handleSubmit = ( form ) => dispatch( recoverPassword( form, () => history.push( '/login' ) ) );

	return (
		<CardFormContainer title="Reestablecer contraseña">
			<ResetPasswordForm onSubmit={handleSubmit} isLoading={isLogging} />
			<Divider />
			<Row justify="center">
				<Link to="/login" style={{ display: 'flex', alignSelf: 'center' }}>Iniciar sesión</Link>
			</Row>
		</CardFormContainer>
	);
};

export default ResetPasswordScreen;
