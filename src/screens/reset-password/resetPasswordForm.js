/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import {
	Form, Input, Button
} from 'antd';
import { bool, func } from 'prop-types';
import { validateMessages } from '../../utils/validateMessages';

const layout = {
	labelCol: {
		span: 6
	},
	wrapperCol: {
		span: 12
	}
};

const ResetPasswordForm = ( { onSubmit, isLoading } ) => (
	<Form {...layout} onFinish={onSubmit} validateMessages={validateMessages} name="nest-messages">
		<Form.Item
			name={[ 'email' ]}
			label="Email"
			rules={[
				{
					required: true,
					type: 'email'
				}
			]}
		>
			<Input />
		</Form.Item>
		<Form.Item wrapperCol={{ ...layout.wrapperCol }} style={{ justifyContent: 'center' }}>
			<Button type="primary" htmlType="submit" block size="large" loading={isLoading}>
				Enviar instrucciones
			</Button>
		</Form.Item>
	</Form>
);

ResetPasswordForm.propTypes = {
	onSubmit: func.isRequired,
	isLoading: bool
};

ResetPasswordForm.defaultProps = {
	isLoading: false
};

export default ResetPasswordForm;
