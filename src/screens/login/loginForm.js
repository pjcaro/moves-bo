/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { bool, func } from 'prop-types';
import {
	Form, Input, Button
} from 'antd';
import { validateMessages } from '../../utils/validateMessages';

const layout = {
	labelCol: {
		span: 6
	},
	wrapperCol: {
		span: 12
	}
};

const LoginForm = ( { onSubmit, isLoading } ) => (
	<Form {...layout} onFinish={onSubmit} validateMessages={validateMessages} name="nest-messages">
		<Form.Item
			name={[ 'email' ]}
			label="Email"
			rules={[
				{
					required: true,
					type: 'email'
				}
			]}
		>
			<Input />
		</Form.Item>
		<Form.Item
			name={[ 'password' ]}
			label="Contraseña"
			rules={[
				{
					required: true
				}
			]}
		>
			<Input.Password />
		</Form.Item>
		<Form.Item wrapperCol={{ ...layout.wrapperCol }} style={{ justifyContent: 'center' }}>
			<Button type="primary" htmlType="submit" block size="large" loading={isLoading}>
				Iniciar sesión
			</Button>
		</Form.Item>
	</Form>
);

LoginForm.propTypes = {
	onSubmit: func.isRequired,
	isLoading: bool
};

LoginForm.defaultProps = {
	isLoading: false
};

export default LoginForm;
