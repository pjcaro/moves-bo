import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
	Divider, Row
} from 'antd';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';
import LoginForm from './loginForm';
import { login } from '../../actions/authentication';
import CardFormContainer from '../../containers/card-form-container/cardFormContainer';
import { DASHBOARD_ROUTE } from '../../routing/paths';
import logoMoves from '../../assets/logo-moves.png';
import logosFooter from '../../assets/logos-footer.png';
import logoUNPD from '../../assets/logo-unpd.png';

const LoginScreen = () => {
	const dispatch = useDispatch();
	const history = useHistory();
	const isLogging = useSelector( ( state ) => state.authentication.isLogging );
	const handleSubmit = ( form ) => dispatch( login( form, () => history.push( DASHBOARD_ROUTE ) ) );

	return (
		<CardFormContainer title="Iniciar sesión">
			<LoginForm onSubmit={handleSubmit} isLoading={isLogging} />
			<Divider />
			<Row justify="center">
				<Link to="/reset-password">Recuperar contraseña</Link>
			</Row>
			<Row justify="center">
				<img
					src={logoMoves}
					alt=""
					style={{
						maxHeight: '10em', maxWidth: '50%', marginTop: '2em'
					}}
				/>
			</Row>
			<Row justify="center">
				<img
					src={logosFooter}
					alt=""
					style={{
						maxHeight: '5em', maxWidth: '90%', marginTop: '2em'
					}}
				/>
			</Row>
			<Row justify="center">
				<img
					src={logoUNPD}
					alt=""
					style={{
						maxHeight: '7em', maxWidth: '30%', marginTop: '2em'
					}}
				/>
			</Row>
		</CardFormContainer>
	);
};

export default LoginScreen;
