import React from 'react';
import { Container, Title } from '../../components';
import { TITLE_LEVEL } from '../../utils/constants';

const PageNotFound = () => (
	<Container>
		<Title level={TITLE_LEVEL}>Página no encontrada</Title>
	</Container>
);

export default PageNotFound;
