import React from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router';
import CardFormContainer from '../../containers/card-form-container/cardFormContainer';
import { edtiVehicle } from '../../actions/vehicles';
import { VEHICLES_ROUTE } from '../../routing/paths';
import EditVehicleForm from './editVehicleForm';
import { getIdFromPath } from '../../utils/utils';
import { getVehicleById } from '../../selectors/vehicles';

const EditVehicleScreen = () => {
	const dispatch = useDispatch();
	const history = useHistory();
	const location = useLocation();
	const id = getIdFromPath( location.pathname );
	const vehicle = useSelector( getVehicleById( id ) );
	const isEditingVehicle = useSelector( ( state ) => state.vehicles.isEditingVehicle );

	const handleSubmit = ( form ) => {
		dispatch( edtiVehicle( id, form, () => history.push( VEHICLES_ROUTE ) ) );
	};

	const handleCancel = () => history.goBack();

	return (
		<CardFormContainer title="Editar vehiculo">
			<EditVehicleForm
				onSubmit={handleSubmit}
				onCancel={handleCancel}
				isLoading={isEditingVehicle}
				form={{
					...vehicle,
					typeId: vehicle?.type?.id,
					brandModelId: vehicle?.brandModel?.id,
					visitorId: vehicle?.visitor?.id,
					visitorEmail: vehicle?.visitor.email
				}}
			/>
		</CardFormContainer>
	);
};

export default EditVehicleScreen;
