/* eslint-disable react/jsx-props-no-spreading */
import React, { useEffect } from 'react';
import { bool, func, shape } from 'prop-types';
import {
	Form, Input
} from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { validateMessages } from '../../utils/validateMessages';
import {
	genericRules, getDataOptions, getNameByKey, layoutNewForm
} from '../../utils/utils';
import { Dropdown, FormButtons } from '../../components';
import { getBrandModels, getVehicleTypes, searchVisitor } from '../../actions/vehicles';
import Vehicle from '../../entity/vehicle';

const EditVehicleForm = ( {
	form, onSubmit, onCancel, isLoading
} ) => {
	const [ fm ] = Form.useForm();

	const types = useSelector( ( state ) => state.vehicles.types );
	const brandModels = useSelector( ( state ) => state.vehicles.brandModels );
	const visitorsResult = useSelector( ( state ) => state.vehicles.visitorsResult );
	const isLoadingTypes = useSelector( ( state ) => state.vehicles.isLoadingTypes );
	const isLoadingBrandModel = useSelector( ( state ) => state.vehicles.isLoadingBrandModel );
	const isSearchingVisitor = useSelector( ( state ) => state.vehicles.isSearchingVisitor );
	const dispatch = useDispatch();

	useEffect( () => {
		dispatch( getVehicleTypes() );
		dispatch( getBrandModels( form.typeId ) );
		dispatch( searchVisitor( form.visitorEmail ) );
	}, [] );

	const handleSelectType = ( type ) => {
		fm.setFieldsValue( { brandModelId: null } );
		dispatch( getBrandModels( type ) );
	};
	const handleSearchVisitor = ( keyword ) => dispatch( searchVisitor( keyword ) );

	const getVisitorResultWithVisitorSelected = () => (
		form.visitorId
			? [ ...visitorsResult, { id: form.visitorId, email: form.visitorEmail } ]
			: visitorsResult
	);

	return (
		<Form
			form={fm}
			initialValues={form}
			{...layoutNewForm}
			onFinish={onSubmit}
			validateMessages={validateMessages}
			name="nest-messages"
		>
			<Form.Item
				name={[ 'license' ]}
				label={getNameByKey( 'license' )}
				rules={[ genericRules ]}
			>
				<Input value={form.license} />
			</Form.Item>
			<Form.Item
				name={[ 'typeId' ]}
				label={getNameByKey( 'type' )}
				rules={[ genericRules ]}
			>
				{Dropdown( {
					disabled: isLoadingTypes,
					loading: isLoadingTypes,
					data: getDataOptions( types ),
					placeholder: 'Seleccione un tipo',
					onSelect: handleSelectType
				} )}
			</Form.Item>
			<Form.Item
				name={[ 'brandModelId' ]}
				label={getNameByKey( 'brandModel' )}
				rules={[ genericRules ]}
			>
				{Dropdown( {
					disabled: isLoadingBrandModel,
					loading: isLoadingBrandModel,
					data: getDataOptions( brandModels ),
					placeholder: 'Seleccione marca - modelo'
				} )}
			</Form.Item>
			<Form.Item
				name={[ 'visitorId' ]}
				label={getNameByKey( 'visitor' )}
				rules={[ genericRules ]}
			>
				{Dropdown( {
					loading: isSearchingVisitor,
					withSearch: true,
					data: getVisitorResultWithVisitorSelected()
						.map( ( visitor ) => ( { key: visitor.id, name: visitor.email } ) ),
					placeholder: 'Seleccione un usuario',
					onSearch: handleSearchVisitor
				} )}
			</Form.Item>
			<FormButtons isLoading={isLoading} onCancel={onCancel} />
		</Form>
	);
};

EditVehicleForm.propTypes = {
	form: shape( Vehicle ).isRequired,
	onSubmit: func.isRequired,
	onCancel: func.isRequired,
	isLoading: bool
};

EditVehicleForm.defaultProps = {
	isLoading: false
};

export default EditVehicleForm;
