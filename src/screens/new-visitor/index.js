import React from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import NewVisitorForm from './newVisitorForm';
import CardFormContainer from '../../containers/card-form-container/cardFormContainer';
import { createVisitor } from '../../actions/visitors';

const NewVisitorScreen = () => {
	const dispatch = useDispatch();
	const history = useHistory();
	const isLoading = useSelector( ( state ) => state.visitors.isCreatingVisitor );

	const handleSubmit = ( form ) => dispatch( createVisitor( form, () => history.push( '/visitors' ) ) );

	const handleCancel = () => history.goBack();

	return (
		<CardFormContainer title="Nuevo usuario">
			<NewVisitorForm
				onSubmit={handleSubmit}
				onCancel={handleCancel}
				isLoading={isLoading}
			/>
		</CardFormContainer>
	);
};

export default NewVisitorScreen;
