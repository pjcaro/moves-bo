/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { bool, func } from 'prop-types';
import {
	Form, Input
} from 'antd';
import { validateMessages } from '../../utils/validateMessages';
import { layoutNewForm } from '../../utils/utils';
import { FormButtons } from '../../components';

const NewVisitorForm = ( { onSubmit, onCancel, isLoading } ) => (
	<Form
		{...layoutNewForm}
		onFinish={onSubmit}
		validateMessages={validateMessages}
		name="nest-messages"
	>
		<Form.Item
			name={[ 'email' ]}
			label="Email"
			rules={[
				{
					required: true,
					type: 'email'
				}
			]}
		>
			<Input />
		</Form.Item>
		<FormButtons isLoading={isLoading} onCancel={onCancel} />
	</Form>
);

NewVisitorForm.propTypes = {
	onSubmit: func.isRequired,
	onCancel: func.isRequired,
	isLoading: bool
};

NewVisitorForm.defaultProps = {
	isLoading: false
};

export default NewVisitorForm;
