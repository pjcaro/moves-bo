import { Card, Spin } from 'antd';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import GoogleMapReact from 'google-map-react';
import {
	BarGraph, CircleGraph, KPICard, KPIFilter, Loading, Title
} from '../../components';
import styles from './styles';
import { getVehicles } from '../../actions/vehicles';
import { getReports, getReportsFiltered } from '../../actions/reports';
import { optionsMap } from '../../utils/utils';

const DashboardScreen = () => {
	const dispatch = useDispatch();
	const vehicles = useSelector( ( state ) => state.vehicles.vehicles );
	const emission = useSelector( ( state ) => state.reports.emission );
	const energeticConsumption = useSelector( ( state ) => state.reports.energeticConsumption );
	const operativeCosts = useSelector( ( state ) => state.reports.operativeCosts );
	const hoursOfUse = useSelector( ( state ) => state.reports.hoursOfUse );
	const avoidedEmission = useSelector( ( state ) => state.reports.avoidedEmission );
	const energeticConsumptionFiltered = useSelector( ( state ) => (
		state.reports.energeticConsumptionFiltered
	) );
	const expenses = useSelector( ( state ) => state.reports.expenses );
	const loadDistribution = useSelector( ( state ) => state.reports.loadDistribution );
	const areasCovered = useSelector( ( state ) => state.reports.areasCovered );
	const kilometers = useSelector( ( state ) => state.reports.kilometers );
	const travelEvents = useSelector( ( state ) => state.reports.travelEvents );
	const chargeTime = useSelector( ( state ) => state.reports.chargeTime );
	const gForceEvents = useSelector( ( state ) => state.reports.gForceEvents );
	const isLoadingReports = useSelector( ( state ) => state.reports.isLoadingReports );
	const isLoadingReportsFiltered = useSelector(
		( state ) => state.reports.isLoadingReportsFiltered
	);

	useEffect( () => {
		dispatch( getReports() );
		dispatch( getReportsFiltered() );
		dispatch( getVehicles() );
	}, [] );

	const handleChangeFilters = ( filters ) => {
		const licenses = filters?.licenses;
		const from = filters?.dates ? filters.dates[ 0 ] : null;
		const to = filters?.dates ? filters.dates[ 1 ] : null;
		dispatch( getReportsFiltered( {
			vehicles: licenses,
			from: from?.toISOString(),
			to: to?.toISOString()
		} ) );
	};

	const handleChangeText = ( keyword ) => dispatch( getVehicles( { keyword } ) );

	return (
		<section style={styles.container}>
			<Spin spinning={isLoadingReports} indicator={<Loading />}>
				<Card style={styles.card}>
					<Title level={3}>Emisiones de CO2 evitadas - Total</Title>
					<div style={styles.cardBody}>
						{emission?.data?.map( ( report ) => (
							<div key={report.title} style={styles.cardBodyContent}>
								<KPICard title={report.title} value={report.value} unit={report.measurementUnit} />
							</div>
						) )}
					</div>
				</Card>
			</Spin>
			<Spin spinning={isLoadingReports} indicator={<Loading />}>
				<Card style={styles.card}>
					<Title level={3}>Consumo energético total</Title>
					<div style={styles.cardBody}>
						{energeticConsumption?.data?.map( ( report ) => (
							<div key={report.title} style={styles.cardBodyContent}>
								<KPICard title={report.title} value={report.value} unit={report.measurementUnit} />
							</div>
						) )}
					</div>
				</Card>
			</Spin>
			<Spin spinning={isLoadingReports} indicator={<Loading />}>
				<Card style={styles.card}>
					<Title level={3}>Costo operacional total</Title>
					<div style={styles.cardBody}>
						{operativeCosts?.data?.map( ( report ) => (
							<div key={report.title} style={styles.cardBodyContent}>
								<KPICard title={report.title} value={report.value} unit={report.measurementUnit} />
							</div>
						) )}
					</div>
				</Card>
			</Spin>
			<Card style={styles.card}>
				<KPIFilter
					onChangeFilters={handleChangeFilters}
					results={vehicles}
					onChangeText={handleChangeText}
				/>
			</Card>
			<Spin spinning={isLoadingReportsFiltered} indicator={<Loading />}>
				<Card style={styles.card}>
					<Title level={3}>Distribución de carga</Title>
					<CircleGraph data={loadDistribution.data} unit={loadDistribution.axisY} />
				</Card>
			</Spin>
			<Spin spinning={isLoadingReportsFiltered} indicator={<Loading />}>
				<Card style={styles.card}>
					<Title level={3}>Tiempo de carga</Title>
					<div style={styles.cardBody}>
						{chargeTime?.data?.map( ( report ) => (
							<div key={report.title} style={styles.cardBodyContent}>
								<KPICard title={report.title} value={report.value} unit={report.measurementUnit} />
							</div>
						) )}
					</div>
				</Card>
			</Spin>
			<Spin spinning={isLoadingReportsFiltered} indicator={<Loading />}>
				<Card style={styles.card}>
					<Title level={3}>Gastos</Title>
					<BarGraph
						data={expenses.data}
						barsWithColors
						unit={expenses.axisY}
					/>
				</Card>
			</Spin>
			<Spin spinning={isLoadingReportsFiltered} indicator={<Loading />}>
				<Card style={styles.card}>
					<Title level={3}>Cantidad de vehículos en uso</Title>
					<BarGraph
						data={hoursOfUse.data}
						withLabel
						unit="vhs"
					/>
				</Card>
			</Spin>
			<Spin spinning={isLoadingReportsFiltered} indicator={<Loading />}>
				<Card style={styles.card}>
					<Title level={3}>Aceleración y frenado</Title>
					<div style={styles.cardBody}>
						{travelEvents?.data?.map( ( report ) => (
							<div key={report.title} style={styles.cardBodyContent}>
								<KPICard title={report.title} value={report.value} unit={report.measurementUnit} />
							</div>
						) )}
					</div>
				</Card>
			</Spin>
			<Spin spinning={isLoadingReportsFiltered} indicator={<Loading />}>
				<Card style={styles.card}>
					<Title level={3}>Cantidad de eventos de fuerza G</Title>
					<div style={styles.cardBody}>
						{gForceEvents?.data?.map( ( report ) => (
							<div key={report.title} style={styles.cardBodyContent}>
								<KPICard title={report.title} value={report.value} unit={report.measurementUnit} />
							</div>
						) )}
					</div>
				</Card>
			</Spin>
			<Spin spinning={isLoadingReportsFiltered} indicator={<Loading />}>
				<Card style={styles.card}>
					<Title level={3}>Consumo energético</Title>
					<div style={styles.cardBody}>
						{energeticConsumptionFiltered?.data?.map( ( report ) => (
							<div key={report.title} style={styles.cardBodyContent}>
								<KPICard title={report.title} value={report.value} unit={report.measurementUnit} />
							</div>
						) )}
					</div>
				</Card>
			</Spin>
			<Spin spinning={isLoadingReportsFiltered} indicator={<Loading />}>
				<Card style={styles.card}>
					<Title level={3}>CO2 evitado y su parametrización</Title>
					<div style={styles.cardBody}>
						{avoidedEmission?.data?.map( ( report ) => (
							<div key={report.title} style={styles.cardBodyContent}>
								<KPICard title={report.title} value={report.value} unit={report.measurementUnit} />
							</div>
						) )}
					</div>
				</Card>
			</Spin>
			<Spin spinning={isLoadingReportsFiltered} indicator={<Loading />}>
				<Card style={styles.card}>
					<Title level={3}>Kilómetros recorridos</Title>
					<div style={styles.cardBody}>
						{kilometers?.data?.map( ( report ) => (
							<div key={report.title} style={styles.cardBodyContent}>
								<KPICard title={report.title} value={report.value} unit={report.measurementUnit} />
							</div>
						) )}
					</div>
				</Card>
			</Spin>
			<Spin spinning={isLoadingReportsFiltered} indicator={<Loading />}>
				<Card style={styles.card}>
					<Title level={3}>Zonas recorridas</Title>
					<div style={{ width: '100%', height: '500px' }}>
						<GoogleMapReact
							bootstrapURLKeys={{ key: process.env.REACT_APP_GOOGLE_API_KEY, libraries: [ 'visualization' ] }}
							center={{ ...areasCovered.center }}
							zoom={areasCovered.zoom}
							heatmap={{
								positions: areasCovered.data,
								options: optionsMap
							}}
						/>
					</div>
				</Card>
			</Spin>
		</section>
	);
};

export default DashboardScreen;
