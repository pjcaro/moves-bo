const styles = {
	container: {
		flex: 1,
		display: 'flex',
		flexDirection: 'column'
	},
	card: {
		marginBottom: '10px'
	},
	cardBody: {
		display: 'flex',
		flexDirection: 'row',
		flexWrap: 'wrap',
		justifyContent: 'space-around',
		alignItems: 'center'
	},
	cardBodyContent: {
		flex: '0 0 33.3%'
	}
};

export default styles;
