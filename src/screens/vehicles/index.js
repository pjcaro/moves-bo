import { Button, Table } from 'antd';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import { getVehicles } from '../../actions/vehicles';
import { baseButtonStyles } from '../../assets/baseStyles';
import {
	Container, Filter, Loading, Title
} from '../../components/index';
import { EDIT_VEHICLE_ROUTE, NEW_VEHICLE_ROUTE } from '../../routing/paths';
import { vehicleColumn } from '../../utils/columns';
import { TITLE_LEVEL } from '../../utils/constants';
import { initialFilters, localeText } from '../../utils/utils';

const VehiclesScreen = () => {
	const [ filters, setFilters ] = useState( initialFilters );
	const vehicles = useSelector( ( state ) => state.vehicles.vehicles );
	const pagination = useSelector( ( state ) => state.vehicles.pagination );
	const isLoadingVehicles = useSelector( ( state ) => state.vehicles.isLoadingVehicles );
	const dispatch = useDispatch();
	const history = useHistory();

	useEffect( () => {
		dispatch( getVehicles() );
	}, [] );

	const handleTableChange = ( pag ) => {
		const { current: page, pageSize } = pag;
		dispatch( getVehicles( { page, pageSize, ...filters } ) );
	};

	const handleChangeFilters = ( flts ) => {
		setFilters( flts );
		dispatch( getVehicles( { ...flts } ) );
	};

	const handleDelete = () => {};
	const handleEdit = ( id ) => history.push( EDIT_VEHICLE_ROUTE.replace( ':id', id ) );

	return (
		<Container>
			<Title level={TITLE_LEVEL}>Vehículos</Title>
			<Filter
				onChange={handleChangeFilters}
			/>
			<Table
				columns={vehicleColumn}
				rowKey={( record ) => record.id}
				dataSource={vehicles.map( ( vehicle ) => ( {
					...vehicle,
					type: vehicle.type?.description,
					brandModel: vehicle.brandModel?.description,
					email: vehicle.visitor?.email,
					onDelete: () => handleDelete( vehicle.id ),
					onEdit: () => handleEdit( vehicle.id )
				} ) )}
				pagination={pagination}
				loading={{ spinning: isLoadingVehicles, indicator: <Loading /> }}
				onChange={handleTableChange}
				locale={localeText}
			/>
			<Button type="primary" style={baseButtonStyles}>
				<Link to={NEW_VEHICLE_ROUTE}>Nuevo</Link>
			</Button>
		</Container>
	);
};

export default VehiclesScreen;
