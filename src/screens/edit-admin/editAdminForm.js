/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { bool, func, shape } from 'prop-types';
import {
	Form, Input
} from 'antd';
import { validateMessages } from '../../utils/validateMessages';
import { layoutNewForm } from '../../utils/utils';
import { Admin } from '../../entity';
import { FormButtons } from '../../components';

const EditAdminForm = ( {
	form, onSubmit, isLoading, onCancel
} ) => (
	<Form
		initialValues={form}
		{...layoutNewForm}
		onFinish={onSubmit}
		validateMessages={validateMessages}
		name="nest-messages"
	>
		<Form.Item
			name={[ 'firstName' ]}
			label="Nombre"
			rules={[
				{
					required: true
				}
			]}
		>
			<Input value={form.firstName} />
		</Form.Item>
		<Form.Item
			name={[ 'lastName' ]}
			label="Apellido"
			rules={[
				{
					required: true
				}
			]}
		>
			<Input value={form.lastName} />
		</Form.Item>
		<FormButtons isLoading={isLoading} onCancel={onCancel} />
	</Form>
);

EditAdminForm.propTypes = {
	form: shape( Admin ).isRequired,
	onSubmit: func.isRequired,
	onCancel: func.isRequired,
	isLoading: bool
};

EditAdminForm.defaultProps = {
	isLoading: false
};

export default EditAdminForm;
