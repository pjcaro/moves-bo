import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router';
import { editAdmin } from '../../actions/admins';
import CardFormContainer from '../../containers/card-form-container/cardFormContainer';
import { ADMINS_ROUTE } from '../../routing/paths';
import { getAdminById } from '../../selectors/admins';
import { getIdFromPath } from '../../utils/utils';
import EditAdminForm from './editAdminForm';

const EditAdminScreen = () => {
	const isEditingAdmin = useSelector( ( state ) => state.admins.isEditingAdmin );
	const location = useLocation();
	const id = getIdFromPath( location.pathname );
	const admin = useSelector( getAdminById( id ) );
	const dispatch = useDispatch();
	const history = useHistory();

	const handleSubmit = ( form ) => (
		dispatch( editAdmin( id, form, () => history.push( ADMINS_ROUTE ) ) )
	);

	const handleCancel = () => history.goBack();

	return (
		<CardFormContainer title="Editar administrador">
			<EditAdminForm
				form={admin}
				onSubmit={handleSubmit}
				onCancel={handleCancel}
				isLoading={isEditingAdmin}
			/>
		</CardFormContainer>
	);
};

export default EditAdminScreen;
