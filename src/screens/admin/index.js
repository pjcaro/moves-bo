import { Button, Table } from 'antd';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import { deleteAdmin, getAdmins } from '../../actions/admins';
import { baseButtonStyles } from '../../assets/baseStyles';
import {
	Container, Filter, Loading, Title
} from '../../components';
import { EDIT_ADMIN_ROUTE, NEW_ADMIN_ROUTE } from '../../routing/paths';
import { adminColumn } from '../../utils/columns';
import { INVITED_STATUS, TITLE_LEVEL } from '../../utils/constants';
import { initialFilters, localeText, status } from '../../utils/utils';

const AdminsScreen = () => {
	const [ filters, setFilters ] = useState( initialFilters );
	const dispatch = useDispatch();
	const admins = useSelector( ( state ) => state.admins.admins );
	const isLoadingAdmins = useSelector( ( state ) => state.admins.isLoadingAdmins );
	const pagination = useSelector( ( state ) => state.admins.pagination );
	const history = useHistory();

	useEffect( () => {
		dispatch( getAdmins() );
	}, [] );

	const handleTableChange = ( pag ) => {
		const { current: page, pageSize } = pag;
		dispatch( getAdmins( { page, pageSize, ...filters } ) );
	};

	const handleChangeFilters = ( flts ) => {
		setFilters( { ...flts } );
		dispatch( getAdmins( { ...flts } ) );
	};

	const handleDelete = ( id ) => dispatch( deleteAdmin( id ) );
	const handleEdit = ( id ) => history.push( EDIT_ADMIN_ROUTE.replace( ':id', id ) );

	return (
		<Container>
			<Title level={TITLE_LEVEL}>Administradores</Title>
			<Filter
				onChange={handleChangeFilters}
				status={status.filter( ( st ) => st.key !== INVITED_STATUS )}
			/>
			<Table
				columns={adminColumn}
				rowKey={( record ) => record.id}
				dataSource={admins.map( ( admin ) => ( {
					...admin,
					onDelete: () => handleDelete( admin.id ),
					onEdit: () => handleEdit( admin.id )
				} ) )}
				pagination={pagination}
				loading={{ spinning: isLoadingAdmins, indicator: <Loading /> }}
				onChange={handleTableChange}
				locale={localeText}
			/>
			<Button type="primary" style={baseButtonStyles}>
				<Link to={NEW_ADMIN_ROUTE}>Nuevo</Link>
			</Button>
		</Container>
	);
};

export default AdminsScreen;
