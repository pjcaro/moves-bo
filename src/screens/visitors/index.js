import {
	Button, Table
} from 'antd';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import { deleteVisitor, getVisitors } from '../../actions/visitors';
import { baseButtonStyles } from '../../assets/baseStyles';
import {
	Container, Filter, Loading, Title
} from '../../components';
import { EDIT_VISTOR_ROUTE, NEW_VISITOR_ROUTE } from '../../routing/paths';
import { visitorColumn } from '../../utils/columns';
import { TITLE_LEVEL } from '../../utils/constants';
import { initialFilters, localeText, status } from '../../utils/utils';

const VisitorsScreen = () => {
	const [ filters, setFilters ] = useState( initialFilters );
	const visitors = useSelector( ( state ) => state.visitors.visitors );
	const isLoadingVisitors = useSelector( ( state ) => state.visitors.isLoadingVisitors );
	const pagination = useSelector( ( state ) => state.visitors.pagination );
	const history = useHistory();

	const dispatch = useDispatch();

	useEffect( () => {
		dispatch( getVisitors() );
	}, [] );

	const handleTableChange = ( pag ) => {
		const { current: page, pageSize } = pag;
		dispatch( getVisitors( { page, pageSize, ...filters } ) );
	};

	const handleChangeFilters = ( flts ) => {
		setFilters( flts );
		dispatch( getVisitors( { ...flts } ) );
	};

	const handleDelete = ( id ) => dispatch( deleteVisitor( id ) );

	const handleEdit = ( id ) => history.push( EDIT_VISTOR_ROUTE.replace( ':id', id ) );

	return (
		<Container>
			<Title level={TITLE_LEVEL}>Usuarios</Title>
			<Filter onChange={handleChangeFilters} status={status} />
			<div>
				<Table
					columns={visitorColumn}
					rowKey={( record ) => record.id}
					dataSource={visitors.map( ( visitor ) => ( {
						...visitor,
						onDelete: () => handleDelete( visitor.id ),
						onEdit: () => handleEdit( visitor.id )
					} ) )}
					pagination={pagination}
					loading={{ spinning: isLoadingVisitors, indicator: <Loading /> }}
					onChange={handleTableChange}
					locale={localeText}
				/>
			</div>
			<Button type="primary" style={baseButtonStyles}>
				<Link to={NEW_VISITOR_ROUTE}>Nuevo</Link>
			</Button>
		</Container>
	);
};

export default VisitorsScreen;
