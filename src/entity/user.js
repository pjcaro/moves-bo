import Person from './person';

export default class User extends Person {
	constructor( id, firstName, lastName, email, role ) {
		super( id, firstName, lastName, email );
		this.role = role;
	}

	static fromJSON( properties ) {
		return new User(
			properties?.id ?? null,
			properties?.firstName ?? null,
			properties?.lastName ?? null,
			properties?.email ?? null,
			properties?.role ?? null
		);
	}
}
