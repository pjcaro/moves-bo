import { BrandModel, VehicleType, Visitor } from '.';

export default class Vehicle {
	constructor( id, type, license, brandModel, visitor ) {
		this.id = id;
		this.type = type;
		this.license = license;
		this.brandModel = brandModel;
		this.visitor = visitor;
	}

	static fromJSON( properties ) {
		return new Vehicle(
			properties?.id ?? '',
			properties?.type ? VehicleType.fromJSON( properties.type ) : {},
			properties?.license ?? '',
			properties?.brandModel ? BrandModel.fromJSON( properties.brandModel ) : {},
			properties?.visitor ? Visitor.fromJSON( properties.visitor ) : {}
		);
	}
}
