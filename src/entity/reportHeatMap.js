import { parserPoint } from '../utils/parsers';
import { initialCenter, initialZoom } from '../utils/utils';
import Report from './report';

export default class ReportHeatMap extends Report {
	constructor( axisY, data, center, zoom ) {
		super( axisY, data );
		this.center = center;
		this.zoom = zoom;
	}

	static setInfo( data ) {
		return data.map( ( item ) => parserPoint( item ) );
	}

	static setCenter( point ) {
		return parserPoint( point );
	}

	static fromJSON( properties ) {
		return new ReportHeatMap(
			properties?.axisY ?? '',
			properties?.data ? this.setInfo( properties.data ) : [],
			properties?.center ? this.setCenter( properties.center ) : initialCenter,
			properties?.zoom ?? initialZoom
		);
	}
}
