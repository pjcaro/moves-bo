import Person from './person';

export default class Visitor extends Person {
	constructor( id, firstName, lastName, email, idCard, status, vehicleLicense ) {
		super( id, firstName, lastName, email );
		this.idCard = idCard;
		this.status = status;
		this.vehicleLicense = vehicleLicense;
	}

	static fromJSON( properties ) {
		return new Visitor(
			properties?.id ?? '',
			properties?.firstName ?? '',
			properties?.lastName ?? '',
			properties?.email ?? '',
			properties?.idCard ?? '',
			properties?.status ?? '',
			properties?.vehicleLicense ?? ''
		);
	}
}
