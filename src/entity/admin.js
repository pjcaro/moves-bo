import Person from './person';

export default class Admin extends Person {
	constructor( id, firstName, lastName, email, status, role ) {
		super( id, firstName, lastName, email );
		this.role = role;
		this.status = status;
	}

	static fromJSON( properties ) {
		return new Admin(
			properties?.id ?? '',
			properties?.firstName ?? '',
			properties?.lastName ?? '',
			properties?.email ?? '',
			properties?.status ?? '',
			properties?.role ?? ''
		);
	}
}
