export default class VehicleType {
	constructor( id, description ) {
		this.id = id;
		this.description = description;
	}

	static fromJSON( properties ) {
		return new VehicleType(
			properties?.id ?? '',
			properties?.description ?? ''
		);
	}
}
