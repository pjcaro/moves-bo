import Person from './person';
import User from './user';
import Visitor from './visitor';
import Admin from './admin';
import VehicleType from './vehicleType';
import BrandModel from './brandModel';
import Report from './report';
import Point from './point';
import ReportHeatMap from './reportHeatMap';

export {
	Person,
	User,
	Visitor,
	Admin,
	VehicleType,
	BrandModel,
	Report,
	Point,
	ReportHeatMap
};
