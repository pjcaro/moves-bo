export default class BrandModel {
	constructor( id, description ) {
		this.id = id;
		this.description = description;
	}

	static fromJSON( properties ) {
		return new BrandModel(
			properties?.id ?? '',
			properties?.description ?? ''
		);
	}
}
