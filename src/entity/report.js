import { parserReportInfo } from '../utils/parsers';

export default class Report {
	constructor( axisY, data ) {
		this.axisY = axisY;
		this.data = data;
	}

	static setInfo( data ) {
		return data.map( ( item ) => parserReportInfo( item ) );
	}

	static fromJSON( properties ) {
		return new Report(
			properties?.axisY ?? '',
			properties?.data ? this.setInfo( properties.data ) : []
		);
	}
}
