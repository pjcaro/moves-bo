export default class Point {
	constructor( lat, lng ) {
		this.lat = lat;
		this.lng = lng;
	}

	static fromJSON( properties ) {
		return new Point(
			properties?.lat ?? null,
			properties?.lng ?? null
		);
	}
}
