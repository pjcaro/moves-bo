export default class ReportInfo {
	constructor( title, value, measurementUnit ) {
		this.title = title;
		this.value = value;
		this.measurementUnit = measurementUnit;
	}

	static fromJSON( properties ) {
		return new ReportInfo(
			properties?.title ?? '',
			properties?.value ?? '',
			properties?.measurementUnit ?? ''
		);
	}
}
