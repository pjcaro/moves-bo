import React from 'react';
import {
	Chart,
	Tooltip,
	Interval
} from 'bizcharts';
import {
	arrayOf, bool, number, shape, string
} from 'prop-types';
import theme from '../../assets/theme';

const BarGraph = ( {
	data, withLabel, barsWithColors, unit
} ) => {
	const scale = {
		value: {
			min: 0,
			formatter: ( val ) => {
				val = `${val} ${unit}`;
				return val;
			}
		},
		cols: {
			value: { alias: unit }
		}
	};

	return (
		<Chart
			data={data}
			padding={[ 60, 20, 40, 60 ]}
			scale={scale}
			autoFit
			height={500}
		>
			<Tooltip
				showTitle={false}
			/>
			<Interval
				position="title*value"
				color={barsWithColors ? [ 'title', [ theme.green, theme.yellow, theme.orange ] ] : theme.blue}
				label={withLabel && [
					'value',
					( val ) => ( {
						content: val,
						style: {
							fill: theme.orange,
							fontSize: 18,
							fontWeight: 'bold'
						}
					} )
				]}
			/>
		</Chart>
	);
};

BarGraph.propTypes = {
	data: arrayOf( shape( {
		title: string,
		value: number
	} ) ).isRequired,
	withLabel: bool,
	barsWithColors: bool,
	unit: string
};

BarGraph.defaultProps = {
	withLabel: false,
	barsWithColors: false,
	unit: ''
};

export default BarGraph;
