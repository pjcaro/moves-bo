import React from 'react';
import { number, string } from 'prop-types';
import Title from '../title';
import styles from './styles';
import Text from '../text';

const KPICard = ( { title, value, unit } ) => {
	if ( value === 0 ) {
		value = '-';
	}
	return (
		<div style={styles.container}>
			<Title level={4}>{title}</Title>
			<Title level={1} style={styles.title}>{ value }</Title>
			<Text strong>{unit}</Text>
		</div>
	);
};

KPICard.propTypes = {
	title: string.isRequired,
	value: number.isRequired,
	unit: string.isRequired
};

export default KPICard;
