const styles = {
	container: {
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		textAlign: 'center'
	},
	title: { marginTop: 0 }
};

export default styles;
