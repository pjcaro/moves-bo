import React from 'react';
import S from './styles';
import logo from '../../assets/logo.png';

const Loading = () => (
	<S.Logo alt="loading" src={logo} className="ant-spin-dot ant-spin-dot-spin" />
);

export default Loading;
