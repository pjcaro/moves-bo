import styled, { keyframes } from 'styled-components';

const rotation = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
`;

const Rotate = styled.div`
	animation: ${rotation} 2s linear infinite;
	display: flex;
	justify-content: center;
	align-items: center
`;

const Logo = styled.img`
	width: 30px;
	height: 30px;
`;

export default {
	Rotate,
	Logo
};
