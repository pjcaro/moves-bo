import { Empty, Select, Tag } from 'antd';
import {
	arrayOf, bool, func, number, oneOfType, shape, string
} from 'prop-types';
import React from 'react';
import { NOT_RESULT_FOUND } from '../../utils/constants';
import { getColorByStatus, localeText } from '../../utils/utils';

const { Option } = Select;

const Dropdown = ( {
	data,
	withSearch,
	onSearch,
	onSelect,
	placeholder,
	loading,
	disabled,
	showTagRender,
	mode,
	allowClear
} ) => {
	const tagRender = ( {
		label, onClose
	} ) => (
		<Tag
			color={getColorByStatus( label )}
			closable
			onClose={onClose}
			style={{ marginRight: 3 }}
		>
			{label}
		</Tag>
	);

	return (
		<Select
			mode={mode}
			tagRender={showTagRender && tagRender}
			allowClear={allowClear}
			showArrow={!withSearch}
			disabled={disabled}
			loading={loading}
			showSearch={withSearch}
			onSearch={onSearch}
			onSelect={onSelect}
			placeholder={placeholder}
			optionFilterProp="children"
			filterOption={( input, option ) => (
				option.children.toLowerCase().indexOf( input.toLowerCase() ) >= 0
			)}
			locale={localeText}
			notFoundContent={
				<Empty description={false} image={Empty.PRESENTED_IMAGE_SIMPLE}>{NOT_RESULT_FOUND}</Empty>
			}
		>
			{data.map( ( item ) => (
				<Option key={item.key || item} value={item.key || item}>{item.name || item}</Option>
			) )}
		</Select>
	);
};

Dropdown.propTypes = {
	data: arrayOf( shape( {
		key: oneOfType( [ number, string ] ),
		name: string
	} ) ).isRequired,
	withSearch: bool,
	onSearch: func,
	onSelect: func,
	placeholder: string.isRequired,
	loading: bool,
	disabled: bool,
	showTagRender: bool,
	mode: string,
	allowClear: bool
};

Dropdown.defaultProps = {
	withSearch: false,
	onSearch: () => {},
	onSelect: () => {},
	loading: false,
	disabled: false,
	showTagRender: false,
	mode: null,
	allowClear: false
};

export default Dropdown;
