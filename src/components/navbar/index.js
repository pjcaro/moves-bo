import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Layout, Button } from 'antd';
import { useHistory } from 'react-router-dom';

import { getUserManualLink, logout } from '../../actions/authentication';

import styles from './styles';

const { Header } = Layout;

const Navbar = () => {
	const history = useHistory();
	const dispatch = useDispatch();
	const userManualLink = useSelector( ( state ) => state.authentication.userManualLink );

	useEffect( () => {
		dispatch( getUserManualLink() );
	}, [] );

	const handleLogoutClick = () => {
		history.push( '/login' );
		return dispatch( logout() );
	};

	return (
		<Header style={styles.header}>
			{/* eslint-disable-next-line max-len */}
			<Button type="link" href={userManualLink} target="_blank">
				Manual de uso de la plataforma
			</Button>
			<Button style={styles.headerItem} type="primary" ghost onClick={handleLogoutClick}>Cerrar sesión</Button>
		</Header>
	);
};

export default Navbar;
