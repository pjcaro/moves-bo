export default {
	header: {
		display: 'flex',
		padding: 10,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'flex-end'
	},
	headerItem: {
		marginLeft: '2rem'
	}
};
