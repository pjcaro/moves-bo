import React from 'react';
import { Form, Button } from 'antd';
import { bool, func } from 'prop-types';
import { baseFormStyles } from '../../assets/baseStyles';

const FormButtons = ( { isLoading, onCancel } ) => (
	<Form.Item style={baseFormStyles.formItem}>
		<div style={baseFormStyles.buttonsContainer}>
			<Button type="primary" size="large" htmlType="submit" loading={isLoading} style={baseFormStyles.button}>
				Guardar
			</Button>
			<Button type="primary" size="large" onClick={onCancel}>
				Cancelar
			</Button>
		</div>
	</Form.Item>
);

FormButtons.propTypes = {
	isLoading: bool.isRequired,
	onCancel: func.isRequired
};

export default FormButtons;
