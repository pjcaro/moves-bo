import React from 'react';
import { Button, DatePicker, Form } from 'antd';
import { arrayOf, func, shape } from 'prop-types';
import debounce from 'lodash.debounce';
import Dropdown from '../dropdown';
import { getNameByKey } from '../../utils/utils';

const { RangePicker } = DatePicker;

const KPIFilter = ( { onChangeFilters, results, onChangeText } ) => {
	const [ form ] = Form.useForm();

	return (
		<div>
			<Form
				form={form}
				onFinish={onChangeFilters}
				style={{ display: 'flex' }}
			>
				<Form.Item
					label={getNameByKey( 'dates' )}
					name="dates"
					style={{ flex: 1, marginRight: '10px' }}
				>
					<RangePicker />
				</Form.Item>
				<Form.Item
					label={getNameByKey( 'license' )}
					name="licenses"
					style={{ flex: 1.5, marginRight: '10px' }}
				>
					{Dropdown( {
						mode: 'multiple',
						withSearch: true,
						data: results.map( ( item ) => ( { key: item.id, name: item.license } ) ),
						onSearch: debounce( onChangeText, 500 ),
						allowClear: true
					} )}
				</Form.Item>
				<Form.Item>
					<Button type="primary" htmlType="submit" loading={false} style={{ marginRight: '10px' }}>
						Aplicar
					</Button>
					<Button
						type="link"
						htmlType="submit"
						loading={false}
						onClick={() => form.resetFields()}
					>
						Limpiar
					</Button>
				</Form.Item>
			</Form>
		</div>
	);
};

KPIFilter.propTypes = {
	onChangeFilters: func.isRequired,
	results: arrayOf( shape( {} ) ),
	onChangeText: func.isRequired
};

KPIFilter.defaultProps = {
	results: []
};

export default KPIFilter;
