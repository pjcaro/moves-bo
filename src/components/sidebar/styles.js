import theme from '../../assets/theme';

const styles = {
	logoSection: {
		height: '32px',
		margin: '16px 24px',
		display: 'flex'
	},
	logo: {
		maxWidth: '100%',
		maxHeight: '100%'
	},
	title: {
		color: theme.whiteOpacity,
		marginLeft: '10px'
	}
};

export default styles;
