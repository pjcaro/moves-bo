import React, { useState, useEffect } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { Layout, Menu } from 'antd';
import { useSelector } from 'react-redux';
import {
	TeamOutlined, HomeOutlined, CarOutlined, AuditOutlined
} from '@ant-design/icons';
import styles from './styles';
import { SUPER_ADMIN } from '../../utils/constants';
import {
	ADMINS_ROUTE, DASHBOARD_ROUTE, VEHICLES_ROUTE, VISITORS_ROUTE
} from '../../routing/paths';
import logo from '../../assets/logo-title.png';
import logoIcon from '../../assets/logo.png';

const { Sider } = Layout;

const Sidebar = () => {
	let location = useLocation();

	const { role } = useSelector( ( state ) => state.user.user );
	const [ collapsed, setCollapsed ] = useState( false );
	const [ selectedKey, setSelectedKey ] = useState( undefined );

	useEffect( () => {
		setSelectedKey( location.pathname );
	}, [ location ] );

	const onCollapse = () => setCollapsed( !collapsed );

	return (
		<Sider
			collapsible
			collapsed={collapsed}
			onCollapse={onCollapse}
		>
			<section style={styles.logoSection}>
				<img alt="logo" src={collapsed ? logoIcon : logo} style={styles.logo} />
			</section>
			<Menu theme="dark" mode="inline" selectedKeys={[ selectedKey ]}>
				<Menu.Item key={DASHBOARD_ROUTE} icon={<HomeOutlined />}>
					<Link to={DASHBOARD_ROUTE}>Tablero</Link>
				</Menu.Item>
				{role === SUPER_ADMIN && (
					<Menu.Item key={ADMINS_ROUTE} icon={<AuditOutlined />}>
						<Link to={ADMINS_ROUTE}>Administradores</Link>
					</Menu.Item>
				)}
				<Menu.Item key={VISITORS_ROUTE} icon={<TeamOutlined />}>
					<Link to={VISITORS_ROUTE}>Usuarios</Link>
				</Menu.Item>
				<Menu.Item key={VEHICLES_ROUTE} icon={<CarOutlined />}>
					<Link to={VEHICLES_ROUTE}>Vehículos</Link>
				</Menu.Item>
			</Menu>
		</Sider>
	);
};

export default Sidebar;
