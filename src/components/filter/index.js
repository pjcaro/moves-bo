import React from 'react';
import { Button, Form, Input } from 'antd';
import {
	arrayOf, func, number, oneOfType, shape, string
} from 'prop-types';
import Dropdown from '../dropdown';
import { initialFilters } from '../../utils/utils';

const Filter = ( { onChange, status } ) => {
	const [ form ] = Form.useForm();

	return (
		<div>
			<Form
				form={form}
				initialValues={initialFilters}
				onFinish={onChange}
				style={{ display: 'flex' }}
			>
				<Form.Item
					label="Buscar"
					name="keyword"
					style={{ flex: 2, marginRight: '10px' }}
				>
					<Input allowClear />
				</Form.Item>
				{status.length > 0 && (
					<Form.Item
						label="Estado"
						name="status"
						style={{ flex: 1, marginRight: '10px' }}
					>
						{Dropdown( {
							data: status,
							showTagRender: true,
							mode: 'multiple',
							allowClear: true
						} )}
					</Form.Item>
				)}
				<Form.Item>
					<Button type="primary" htmlType="submit" loading={false} style={{ marginRight: '10px' }}>
						Aplicar
					</Button>
					<Button
						type="link"
						htmlType="submit"
						loading={false}
						onClick={() => form.resetFields()}
					>
						Limpiar
					</Button>
				</Form.Item>
			</Form>
		</div>
	);
};

Filter.propTypes = {
	onChange: func.isRequired,
	status: arrayOf( shape( {
		key: oneOfType( [ string, number ] ),
		name: string
	} ) )
};

Filter.defaultProps = {
	status: []
};

export default Filter;
