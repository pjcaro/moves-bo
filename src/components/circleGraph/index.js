import React from 'react';
import {
	Chart,
	Axis,
	Tooltip,
	Interval,
	Interaction,
	Coordinate,
	Annotation
} from 'bizcharts';
import {
	arrayOf, number, shape, string
} from 'prop-types';
import theme from '../../assets/theme';

const CircleGraph = ( { data, unit } ) => {
	const sumItems = data.reduce( ( acc, item ) => acc + item.value, 0 );
	const scale = {
		value: {
			formatter: ( val ) => {
				val = `${( ( val / sumItems ) * 100 ).toFixed( 0 )}%`;
				return val;
			}
		}
	};

	return (
		<Chart data={data} height={500} autoFit scale={scale}>
			<Coordinate type="theta" radius={0.8} innerRadius={0.75} />
			<Axis visible={false} />
			<Tooltip
				showTitle={false}
			/>
			<Interval
				adjust="stack"
				position="value"
				color={[ 'title', [ theme.green, theme.yellow, theme.orange ] ]}
				shape="sliceShape"
				label={[ 'value', {
					content: ( d ) => `${d.title}: ${d.value} ${unit}`
				} ]}
				style={{
					lineWidth: 10,
					stroke: '#fff'
				}}
			/>
			<Annotation.Text
				position={[ '50%', '45%' ]}
				content={sumItems}
				style={{
					lineHeight: '240px',
					fontSize: '70',
					fill: '#262626',
					textAlign: 'center'
				}}
			/>
			<Annotation.Text
				position={[ '50%', '60%' ]}
				content={unit}
				style={{
					lineHeight: '240px',
					fontSize: '30',
					fill: '#262626',
					textAlign: 'center'
				}}
			/>
			<Interaction type="element-single-selected" />
		</Chart>
	);
};

CircleGraph.propTypes = {
	data: arrayOf( shape( {
		title: string,
		value: number
	} ) ).isRequired,
	unit: string
};

CircleGraph.defaultProps = {
	unit: ''
};

export default CircleGraph;
