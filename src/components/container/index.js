import React from 'react';
import { Card } from 'antd';
import { arrayOf, element, oneOfType } from 'prop-types';
import { styles } from './styles';

const Container = ( { children } ) => (
	<Card style={styles.container}>
		{children}
	</Card>
);

Container.propTypes = {
	children: oneOfType( [
		arrayOf( element ),
		element
	] ).isRequired
};

export default Container;
