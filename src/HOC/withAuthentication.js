/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';

const withAuthentication = ( WrapperComponent ) => {
	const WithAuthentication = ( props ) => {
		const isLogged = useSelector( ( state ) => state.authentication.isLogged );

		if ( isLogged ) {
			return <WrapperComponent {...props} />;
		}

		return <Redirect to="/login" />;
	};

	return WithAuthentication;
};

export default withAuthentication;
