import React, { useEffect } from 'react';
import { Switch, Route } from 'react-router-dom';
import { Layout } from 'antd';
import { useDispatch, useSelector } from 'react-redux';

import getRoutes from './routing/routes';

import './App.css';
import { Navbar, Sidebar } from './components';
import { styles } from './styles';
import 'antd/dist/antd.css';
import { getContractLink } from './actions/authentication';
import logosFooter from './assets/logos-footer.png';

const { Footer, Content } = Layout;

const App = () => {
	const { role } = useSelector( ( state ) => state.user.user );
	const isLogged = useSelector( ( state ) => state.authentication.isLogged );
	const contractLink = useSelector( ( state ) => state.authentication.contractLink );
	const dispatch = useDispatch();

	useEffect( () => {
		dispatch( getContractLink() );
	}, [] );

	return (
		<Layout style={styles.container}>
			{isLogged && <Sidebar />}
			<Layout className="site-layout">
				{isLogged && <Navbar />}
				<Content style={styles.content}>
					<Switch>
						{
							getRoutes( role ).map( ( route ) => (
								<Route
									key={route.path}
									path={route.path}
									component={route.component}
									exact={route.exact}
								/>
							) )
						}
					</Switch>
				</Content>
				<Footer style={styles.footer}>
					{ isLogged && <img src={logosFooter} alt="" style={{ maxHeight: '5em', marginBottom: '2em' }} /> }
					<a href={contractLink}>Dirección de contrato en blockchain</a>
				</Footer>
			</Layout>
		</Layout>
	);
};

export default App;
